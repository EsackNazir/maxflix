package com.maxflix.app;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.maxflix.app.R;
import tcking.github.com.giraffeplayer.GiraffePlayer;

public class FullVideoActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    GiraffePlayer player;
    YouTubePlayerView youTubePlayer;
    private String video_URL = "";
    private String API = "AIzaSyAw1nNVn8q5OShMOsfUWDHR9ESOLLDkZas";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_full_video);

        RelativeLayout griffe_player = (RelativeLayout) findViewById(R.id.giraffe_player_layout);
        griffe_player.setVisibility(View.GONE);
        RelativeLayout youtube_player = (RelativeLayout) findViewById(R.id.youtube_player_layout);
        youtube_player.setVisibility(View.GONE);
        ImageView back = (ImageView) findViewById(R.id.back_arrow);
        back.setVisibility(View.GONE);


        Intent intent = getIntent();
        if (intent != null) {
            video_URL = intent.getStringExtra("video_URL");
            String video_type = intent.getStringExtra("video_type");
            if (video_type.equalsIgnoreCase("1")) {
                griffe_player.setVisibility(View.VISIBLE);
                player = new GiraffePlayer(FullVideoActivity.this);
                player.play(video_URL);
                player.setFullScreenOnly(true);
            } else if (video_type.equalsIgnoreCase("2")) {
                youtube_player.setVisibility(View.VISIBLE);
                back.setVisibility(View.VISIBLE);
                youTubePlayer = (YouTubePlayerView) findViewById(R.id.youtube_view);
                youTubePlayer.initialize(API, FullVideoActivity.this);
            }

        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(API, this);
        }
    }

    private YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.youtube_view);
    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        youTubePlayer.cueVideo(video_URL);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Failured to Initialize!", Toast.LENGTH_LONG).show();
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, 1).show();
        } else {
            @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) String errorMessage = String.format(
                    getString(R.string.error_player), youTubeInitializationResult.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (player != null) {
            player.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (player != null) {
            player.onResume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (player != null) {
            player.onDestroy();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (player != null) {
            player.onConfigurationChanged(newConfig);
        }
    }

    @Override
    public void onBackPressed() {
        if (player != null && player.onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }
}

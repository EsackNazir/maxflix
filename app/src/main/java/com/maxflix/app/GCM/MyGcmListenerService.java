package com.maxflix.app.GCM;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;


import com.maxflix.app.Activities.MainActivity;
import com.maxflix.app.R;

import org.json.JSONException;
import org.json.JSONObject;

public class MyGcmListenerService extends IntentService {
    private static final String TAG = "MyGcmListenerService";
    private int notificationIcon;
    int count;

    public MyGcmListenerService() {
        super("MyIntentService");
    }

   /* @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);

        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        try {
            JSONObject object = new JSONObject(message);
            String mes = object.optString("message");
            count++;
            sendNotification(mes);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }*/


    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle bundle = intent.getExtras();

        String s = bundle.getString("message");
        try {
            JSONObject object = new JSONObject(s);
            String message = object.optString("message");
            String chat = object.optString("type");
            Log.e("s", "" + s);
            Log.e("message", "" + message);
            sendNotification(message, chat);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void sendNotification(String message, String chat) {

            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationBuilder.setColor(Color.BLACK);
                notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
            } else {
                notificationBuilder.setColor(Color.BLACK);
                notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
            }
            notificationBuilder.setContentTitle(this.getResources().getString(R.string.app_name))
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri).setTicker(message)
                    .setContentIntent(pendingIntent).setNumber(count);

            notificationManager.notify(count /* ID of notification */, notificationBuilder.build());
            Log.e("count", "" + count);


        }
    }




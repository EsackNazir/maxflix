package com.maxflix.app.Utils;

public class URLUtils {
    //public static String base = "http://startstreaming.info/";
    // public static String base = "http://default.startstreaming.info/";
    public static String base = "http://theriflix.appoets.co/";


    public static String login = base + "userApi/login";
    public static String register = base + "userApi/register";
    public static String forgot_password = base + "userApi/forgotpassword";
    public static String profile_update = base + "userApi/updateProfile";
    public static String change_Password = base + "userApi/changePassword";
    public static String categories = base + "userApi/categories";
    public static String home = base + "userApi/home";
    public static String categoryVideos = base + "userApi/categoryVideos";
    public static String getWishlist = base + "userApi/getWishlist";
    public static String deleteWishlist = base + "userApi/deleteWishlist";
    public static String addWishlist = base + "userApi/addWishlist";
    public static String getHistory = base + "userApi/getHistory";
    public static String deleteHistory = base + "userApi/deleteHistory";
    public static String singleVideo = base + "userApi/singleVideo";
    public static String userRating = base + "userApi/userRating";
    public static String searchVideo = base + "userApi/searchVideo";
    public static String common = base + "userApi/common";
    public static String deleteAccount = base + "userApi/deleteAccount";
    public static String subCategoryVideos = base + "userApi/subCategoryVideos";
    public static String addHistory = base + "userApi/addHistory";
    public static String settings = base + "userApi/settings";

}

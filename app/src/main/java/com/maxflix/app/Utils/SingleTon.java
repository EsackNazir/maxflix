package com.maxflix.app.Utils;


public class SingleTon {
    private static SingleTon ourInstance = new SingleTon();


    public String GCMKey = "";

    private SingleTon() {
    }

    public static SingleTon getInstance() {
        return ourInstance;
    }
}

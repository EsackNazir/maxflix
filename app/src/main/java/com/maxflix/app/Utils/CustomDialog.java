package com.maxflix.app.Utils;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;

public class CustomDialog extends Dialog {
    public CustomDialog(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }
}

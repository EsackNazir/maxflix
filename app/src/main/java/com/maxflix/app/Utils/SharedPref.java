package com.maxflix.app.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {

    private static SharedPreferences sharedPreferences;

    @SuppressLint("CommitPrefEdits")
    public static void putKey(Context context, String Key, String Value) {
        sharedPreferences = context.getSharedPreferences("", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Key, Value);
        editor.apply();

    }

    public static String getKey(Context contextGetKey, String Key) {
        sharedPreferences = contextGetKey.getSharedPreferences("", Context.MODE_PRIVATE);
        return sharedPreferences.getString(Key, "");

    }

}

package com.maxflix.app.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v13.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.maxflix.app.R;
import com.maxflix.app.Utils.ConnectionHelper;
import com.maxflix.app.Utils.CustomDialog;
import com.maxflix.app.Utils.PostHelper;
import com.maxflix.app.Utils.SharedPref;
import com.maxflix.app.Utils.SingleTon;
import com.maxflix.app.Utils.UIUtils;
import com.maxflix.app.Utils.URLUtils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Login extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {


    private CallbackManager callbackManager;

    private static final int RC_SIGN_IN = 0;
    private GoogleApiClient mGoogleApiClient;
    private boolean mIntentInProgress;
    private boolean mSignInClicked;
    private ConnectionResult mConnectionResult;
    GoogleCloudMessaging gcm;
    /*----------Facebook Login---------------*/

    private ProgressBar progressBar;
    private TextInputLayout email_lay, pass_lay;
    private EditText edt_email, edt_pass;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(Login.this);
        //noinspection deprecation
        AppEventsLogger.activateApp(Login.this);
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);

        Toolbar toolbar = (Toolbar) findViewById(R.id.sign_in_toolbar);
        toolbar.setTitle(R.string.sign_in);
        toolbar.setTitleTextColor(Color.WHITE);
        //setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT > 15) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        String[] PERMISSIONS = {Manifest.permission.READ_CONTACTS,
                Manifest.permission.WRITE_CONTACTS,
                Manifest.permission.GET_ACCOUNTS,Manifest.permission_group.STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                };

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, 1);
        }

        email_lay = (TextInputLayout) findViewById(R.id.layout_email);
        pass_lay = (TextInputLayout) findViewById(R.id.layout_password);

          /*-------------Google Integration----------------- */
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(new Scope(Scopes.PROFILE))
                .addScope(new Scope(Scopes.EMAIL))
                .build();
        mGoogleApiClient.connect();

        Button sign_btn = (Button) findViewById(R.id.submit_btn);
        sign_btn.setOnClickListener(this);
        Button sigUp_btn = (Button) findViewById(R.id.signUp_btn);
        sigUp_btn.setOnClickListener(this);
        Button btn_google = (Button) findViewById(R.id.btn_google_sign);
        btn_google.setOnClickListener(this);


        edt_email = (EditText) findViewById(R.id.ed_email);
        edt_pass = (EditText) findViewById(R.id.ed_password);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);


         /*----------------Face Integration---------------*/
        try {
            @SuppressLint("PackageManagerGetSignatures") PackageInfo info = getPackageManager().getPackageInfo(
                    "com.maxflix.app",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException ignored) {

        }

        getGCMKey();

        /*Facebook*/
        LoginButton fb_LoginButton = (LoginButton) findViewById(R.id.btn_facebook);
        assert fb_LoginButton != null;
        fb_LoginButton.setReadPermissions("public_profile email");
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        if (AccessToken.getCurrentAccessToken() != null) {
                            progressBar.setVisibility(View.VISIBLE);
                            GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(JSONObject object, GraphResponse response) {

                                    Log.d("object", "" + object);
                                    try {

                                        JSONObject fb_object = new JSONObject();
                                        String fb_id = object.getString("id");
                                        Log.e("fb_id", "" + fb_id);
                                        URL fb_profile_image = new URL("https://graph.facebook.com/" + fb_id + "/picture?type=large");
                                        String profile_img = fb_profile_image.toString();
                                        Log.d("fb_profile_image", "" + fb_profile_image.toString());

                                        fb_object.put("device_token", SingleTon.getInstance().GCMKey);
                                        fb_object.put("device_type", "android");
                                        fb_object.put("login_by", "facebook");
                                        fb_object.put("name", object.getString("name"));
                                        fb_object.put("mobile", "");
                                        fb_object.put("password", "");
                                        fb_object.put("picture", profile_img);
                                        fb_object.put("email", object.getString("email"));
                                        fb_object.put("social_unique_id", fb_id);
                                        Log.e("face_book_post_value", "" + fb_object);

                                        PostHelper postHelper = new PostHelper(Login.this);
                                        JSONObject lg_fb_post = postHelper.Post(URLUtils.register, fb_object.toString());

                                        Log.e("fb_object", "" + lg_fb_post);
                                        if (lg_fb_post != null) {

                                            String result = lg_fb_post.optString("success");
                                            if (result.equalsIgnoreCase("true")) {
                                                Intent mainIntent = new Intent(Login.this, MainActivity.class);
                                                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                SharedPref.putKey(Login.this, "login_type", "Logged");
                                                SharedPref.putKey(Login.this, "ID", lg_fb_post.optString("id"));
                                                SharedPref.putKey(Login.this, "PROFILE_IMAGE", lg_fb_post.optString("picture"));
                                                SharedPref.putKey(Login.this, "NAME", lg_fb_post.optString("name"));
                                                SharedPref.putKey(Login.this, "TOKEN", lg_fb_post.optString("token"));
                                                SharedPref.putKey(Login.this, "push_status", lg_fb_post.optString("push_status"));
                                                SharedPref.putKey(Login.this, "user_type", lg_fb_post.optString("user_type"));
                                                SharedPref.putKey(Login.this, "CHANGE_PASS", "Hide");
                                                startActivity(mainIntent);
                                                Login.this.finish();
                                            } else {
                                                UIUtils.showToastMsg(Login.this, lg_fb_post.optString("error"));
                                            }

                                        } else {
                                            UIUtils.showToast(Login.this, R.string.con_timeout);

                                        }
                                        progressBar.setVisibility(View.GONE);


                                    } catch (JSONException | IOException e) {
                                        e.printStackTrace();
                                    }

                                }

                            });
                            Bundle parameters = new Bundle();
                            parameters.putString("fields", "id,name,link,email,picture");
                            request.setParameters(parameters);
                            request.executeAsync();


                        }
                    }

                    @Override
                    public void onCancel() {
                        UIUtils.showToastMsg(Login.this, "Facebook login cancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        //UIUtils.showToast(Login.this, error.getMessage());
                    }
                }

        );

        final TextView forgot = (TextView) findViewById(R.id.forgot_pass);
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CustomDialog forgotDialog = new CustomDialog(Login.this);
                forgotDialog.setContentView(R.layout.forgot_dialog);
                forgotDialog.setCancelable(false);

                final EditText email_text = (EditText) forgotDialog.findViewById(R.id.forgot_email);
                final ProgressBar progressBar = (ProgressBar) forgotDialog.findViewById(R.id.progress_bar);
                progressBar.setVisibility(View.GONE);
                final Button forgot_btn = (Button) forgotDialog.findViewById(R.id.submit);
                final Button ignore = (Button) forgotDialog.findViewById(R.id.ignore);
                ignore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        forgotDialog.dismiss();
                    }
                });

                forgot_btn.setOnClickListener(new View.OnClickListener() {

                    Boolean check = false;

                    @Override
                    public void onClick(View view) {
                        final String email_str = email_text.getText().toString().trim();

                        if (!isValidEmail(email_str)) {
                            UIUtils.showToastMsg(Login.this, "Enter the valid email id.");
                            check = true;
                        }
                        if (!check) {
                            new AsyncTask<JSONObject, JSONObject, JSONObject>() {

                                @Override
                                protected void onPreExecute() {
                                    super.onPreExecute();
                                    progressBar.setVisibility(View.VISIBLE);
                                }

                                @Override
                                protected JSONObject doInBackground(JSONObject... jsonObjects) {
                                    JSONObject object = new JSONObject();
                                    try {
                                        object.put("email", email_str);
                                        PostHelper postHelper = new PostHelper(Login.this);
                                        return postHelper.Post(URLUtils.forgot_password, object.toString());
                                    } catch (JSONException | IOException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(JSONObject jsonObject) {
                                    super.onPostExecute(jsonObject);
                                    progressBar.setVisibility(View.GONE);
                                    if (jsonObject != null) {
                                        if (jsonObject.optString("success").equalsIgnoreCase("true")) {
                                            UIUtils.showToastMsg(Login.this, jsonObject.optString("message"));
                                        }

                                    } else {
                                        UIUtils.showToast(Login.this, R.string.con_timeout);
                                    }
                                }
                            }.execute();
                        }
                        

                    }
                });

                forgotDialog.show();
            }
        });


    }

    private boolean hasPermissions(Login login, String[] permissions) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && login != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(login, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;

    }

    private void getGCMKey() {
        if (ConnectionHelper.isConnectingToInternet(Login.this))
            new AsyncTask<String, String, String>() {
                @Override
                protected String doInBackground(String... params) {

                    if (checkPlayServices()) {
                        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(Login.this);
                        try {
                            if (gcm == null) {
                                gcm = GoogleCloudMessaging.getInstance(Login.this);
                            }
                            //noinspection deprecation
                            SingleTon.getInstance().GCMKey = gcm.register(getResources().getString(R.string.GCM_SENDER));
                            Log.e("RegistrationID", "" + SingleTon.getInstance().GCMKey);

                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                    }

                    return null;
                }
            }.execute();
        else UIUtils.showToast(Login.this, R.string.check_network);
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, 9000)
                        .show();
            } else {
                Log.i("MyDevice", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    private void handleSignResult(final boolean result) {
        if (ConnectionHelper.isConnectingToInternet(Login.this))
            new AsyncTask<JSONObject, JSONObject, JSONObject>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progressBar.setVisibility(ProgressBar.VISIBLE);
                }

                @SuppressWarnings("MissingPermission")
                @Override
                protected JSONObject doInBackground(JSONObject... params) {
                    if (result) {
                        //noinspection deprecation
                        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                            //noinspection deprecation
                            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                            String g_profile_name = currentPerson.getDisplayName();
                            String g_profile_ID = currentPerson.getId();
                            //noinspection deprecation
                            String g_profile_email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                            String str_profile_URL = currentPerson.getImage().getUrl();

                            Log.e("g_name", "" + g_profile_name);
                            Log.e("g_profile_ID", "" + g_profile_ID);
                            Log.e("g_profile_email", "" + g_profile_email);
                            Log.e("str_profile_URL", "" + str_profile_URL);

                            JSONObject object = new JSONObject();
                            try {
                                object.put("device_token", SingleTon.getInstance().GCMKey);
                                object.put("device_type", "android");
                                object.put("login_by", "google");
                                object.put("email", g_profile_email);
                                object.put("password", "");
                                object.put("mobile", "");
                                object.put("picture", str_profile_URL);
                                object.put("social_unique_id", g_profile_ID);
                                object.put("name", g_profile_name);


                                Log.e("google_post_value", "" + object);

                                PostHelper postHelper = new PostHelper(Login.this);
                                return postHelper.Post(URLUtils.register, object.toString());

                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(JSONObject object) {
                    super.onPostExecute(object);
                    progressBar.setVisibility(ProgressBar.GONE);
                    Log.e("Object", "" + object);
                    if (object != null) {
                        String result = object.optString("success");
                        if (result.equalsIgnoreCase("true")) {
                            Intent mainIntent = new Intent(Login.this, MainActivity.class);
                            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            SharedPref.putKey(Login.this, "login_type", "Logged");
                            SharedPref.putKey(Login.this, "ID", object.optString("id"));
                            SharedPref.putKey(Login.this, "PROFILE_IMAGE", object.optString("picture"));
                            SharedPref.putKey(Login.this, "NAME", object.optString("name"));
                            SharedPref.putKey(Login.this, "TOKEN", object.optString("token"));
                            SharedPref.putKey(Login.this, "user_type", object.optString("user_type"));
                            SharedPref.putKey(Login.this, "push_status", object.optString("push_status"));
                            SharedPref.putKey(Login.this, "CHANGE_PASS", "Hide");
                            startActivity(mainIntent);
                            Login.this.finish();

                        } else {
                            UIUtils.showToastMsg(Login.this, object.optString("error"));

                        }
                    } else {
                        UIUtils.showToast(Login.this, R.string.con_timeout);
                    }

                }
            }.execute();
        else UIUtils.showNetworkAlert(Login.this, R.string.check_network);

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_google_sign:
                progressBar.setVisibility(ProgressBar.VISIBLE);
                signInWithGplus();
                break;

            case R.id.submit_btn:
                Boolean flag = false;
                final String str_email, str_pass;
                str_email = edt_email.getText().toString();
                str_pass = edt_pass.getText().toString();

                if (ConnectionHelper.isConnectingToInternet(Login.this)) {
                    if ((!isValidEmail(str_email))) {
                        email_lay.setError("Required Field");
                        flag = true;
                    }
                    if (str_pass.length() < 0) {
                        email_lay.setError(null);
                        pass_lay.setError("Required Field");
                        flag = true;
                    }

                    if (!flag) {
                        email_lay.setError(null);
                        pass_lay.setError(null);
                        new AsyncTask<JSONObject, JSONObject, JSONObject>() {

                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                progressBar.setVisibility(View.VISIBLE);
                            }

                            @Override
                            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                                JSONObject object = new JSONObject();
                                try {
                                    object.put("device_token", SingleTon.getInstance().GCMKey);
                                    object.put("device_type", "android");
                                    object.put("login_by", "manual");
                                    object.put("email", str_email);
                                    object.put("password", str_pass);
                                    object.put("social_unique_id", "");
                                    Log.e("post_value", "" + object);

                                    PostHelper postHelper = new PostHelper(Login.this);
                                    return postHelper.Post(URLUtils.login, object.toString());


                                } catch (JSONException | IOException e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }

                            @Override
                            protected void onPostExecute(JSONObject object) {
                                super.onPostExecute(object);
                                progressBar.setVisibility(View.GONE);
                                if (object != null) {
                                    Log.e("object", "" + object);
                                    String result = object.optString("success");
                                    if (result.equalsIgnoreCase("true")) {
                                        Intent mainIntent = new Intent(Login.this, MainActivity.class);
                                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        SharedPref.putKey(Login.this, "login_type", "Logged");
                                        SharedPref.putKey(Login.this, "ID", object.optString("id"));
                                        SharedPref.putKey(Login.this, "PROFILE_IMAGE", object.optString("picture"));
                                        SharedPref.putKey(Login.this, "NAME", object.optString("name"));
                                        SharedPref.putKey(Login.this, "TOKEN", object.optString("token"));
                                        SharedPref.putKey(Login.this, "user_type", object.optString("user_type"));
                                        SharedPref.putKey(Login.this, "push_status", object.optString("push_status"));
                                        SharedPref.putKey(Login.this, "CHANGE_PASS", "");
                                        startActivity(mainIntent);
                                        Login.this.finish();

                                    } else {
                                        UIUtils.showToastMsg(Login.this, object.optString("error"));
                                    }
                                } else {
                                    UIUtils.showToastMsg(Login.this, String.valueOf(R.string.con_timeout));

                                }

                            }
                        }.execute();
                    }

                } else {
                    UIUtils.showNetworkAlert(Login.this, R.string.check_network);
                }
                break;

            case R.id.signUp_btn:
                Intent intent = new Intent(Login.this, Register.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }

    }

    private void signInWithGplus() {
        if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInError();
        }
    }


    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ConnectionHelper.isConnectingToInternet(Login.this)) {
            mSignInClicked = false;
            // GetHelper user's information
            handleSignResult(true);


        } else {
            UIUtils.showNetworkAlert(Login.this, R.string.check_network);

        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
        handleSignResult(false);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("Google_sign", "onConnectionFailed:" + connectionResult);
        progressBar.setVisibility(ProgressBar.GONE);
        if (!connectionResult.hasResolution()) {
            //noinspection deprecation
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this, 0).show();
            return;
        }
        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            mConnectionResult = connectionResult;

            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to
                // resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }
        // UIUtils.showToastMsg(Login.this, "Google sign canceled");

    }

    private void resolveSignInError() {

        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            if (resultCode != RESULT_OK) {
                mSignInClicked = false;
            }
            mIntentInProgress = false;
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //mGoogleApiClient.connect();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //noinspection deprecation
        AppEventsLogger.activateApp(this);
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //noinspection deprecation
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ConnectionHelper.isConnectingToInternet(Login.this);
    }
}

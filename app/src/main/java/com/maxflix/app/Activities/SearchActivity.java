package com.maxflix.app.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.maxflix.app.R;
import com.maxflix.app.Utils.PostHelper;
import com.maxflix.app.Utils.SharedPref;
import com.maxflix.app.Utils.UIUtils;
import com.maxflix.app.Utils.URLUtils;
import com.maxflix.app.adapters.SimpleAdapter;
import com.maxflix.app.models.SearchList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchActivity extends AppCompatActivity {
    private RelativeLayout relativeLayout;
    private List<SearchList> list = new ArrayList<>();
    SimpleAdapter mAdapter;
    private RecyclerView recyclerView;
    public EditText search;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.forgot_in_toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle("Search");
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        search = (EditText) findViewById(R.id.search_edit_text);

        recyclerView = (RecyclerView) findViewById(R.id.more_recycler_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        relativeLayout = (RelativeLayout) findViewById(R.id.error_layout);
        relativeLayout.setVisibility(View.GONE);

        addTextListener();

        new AsyncTask<JSONObject, JSONObject, JSONObject>() {
            ProgressBar progressBar;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar = (ProgressBar) findViewById(R.id.more_progress_bar);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(SearchActivity.this, "ID"));
                    jsonObject.put("token", SharedPref.getKey(SearchActivity.this, "TOKEN"));
                    Log.e("movie_list", "" + jsonObject);
                    PostHelper postHelper = new PostHelper(SearchActivity.this);
                    return postHelper.Post(URLUtils.searchVideo, jsonObject.toString());
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                progressBar.setVisibility(View.GONE);
                Log.e("check", "" + jsonObject);
                if (jsonObject != null) {
                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        if (jsonArray != null) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.optJSONObject(i);
                                String id = object.optString("admin_video_id");
                                String name = object.optString("title");
                                String image = object.optString("default_image");
                                list.add(new SearchList(id, name, image));
                            }
                            if (list.isEmpty()) {
                                relativeLayout.setVisibility(View.VISIBLE);
                            } else {
                                mAdapter = new SimpleAdapter(list, SearchActivity.this);
                                recyclerView.setAdapter(mAdapter);
                            }

                        } else {
                            UIUtils.showToastMsg(SearchActivity.this, jsonObject.optString("error"));
                            if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                                Intent intent = new Intent(SearchActivity.this, Login.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        }

                    }
                } else {
                    UIUtils.showToast(SearchActivity.this, R.string.con_timeout);
                    relativeLayout.setVisibility(View.VISIBLE);
                }

            }
        }.execute();
    }

    private void addTextListener() {
        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {

                query = query.toString().toLowerCase();

                final List<SearchList> filteredList = new ArrayList<>();

                for (int i = 0; i < list.size(); i++) {

                    final String text = list.get(i).getMovie_name().toLowerCase();
                    if (text.contains(query)) {
                        String id = list.get(i).getId();
                        String name = list.get(i).getMovie_name();
                        String image = list.get(i).getMovie_image();
                        filteredList.add(new SearchList(id, name, image));
                    }
                }

                recyclerView.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
                mAdapter = new SimpleAdapter(filteredList, SearchActivity.this);
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }
        });
    }
}

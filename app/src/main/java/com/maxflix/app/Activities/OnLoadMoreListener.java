package com.maxflix.app.Activities;

/**
 * Created by KrishnaDev on 10/22/16.
 */
public interface OnLoadMoreListener {
    void onLoadMore();
}

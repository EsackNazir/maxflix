package com.maxflix.app.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.maxflix.app.R;
import com.maxflix.app.Utils.GetHelper;
import com.maxflix.app.Utils.PostHelper;
import com.maxflix.app.Utils.SharedPref;
import com.maxflix.app.Utils.UIUtils;
import com.maxflix.app.Utils.URLUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.StringReader;

public class PrivacyActivity extends AppCompatActivity {

    private String type = "";
    private TextView content_msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);

        Toolbar toolbar = (Toolbar) findViewById(R.id.forgot_in_toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setTitleTextColor(Color.WHITE);
        Intent intent = getIntent();
        if (intent != null) {
            toolbar.setTitle(intent.getStringExtra("title"));
            type = intent.getStringExtra("type");
        }
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
        content_msg = (TextView) findViewById(R.id.content_details);

        new AsyncTask<JSONObject, JSONObject, JSONObject>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {

                try {

                    if (type.equalsIgnoreCase("1")) {
                        String url = URLUtils.base + "privacy?"+SharedPref.getKey(PrivacyActivity.this, "ID") + "&" +
                                SharedPref.getKey(PrivacyActivity.this, "TOKEN");

                        GetHelper getHelper = new GetHelper(PrivacyActivity.this);
                        return getHelper.run(url);
                    } else if (type.equalsIgnoreCase("2")) {
                        String url = URLUtils.base + "terms";
                        GetHelper getHelper = new GetHelper(PrivacyActivity.this);
                        return getHelper.run(url);
                    }


                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                progressBar.setVisibility(View.GONE);
                if (jsonObject != null) {
                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {
                        JSONObject content = jsonObject.optJSONObject("page");
                        String msg = content.optString("content");
                        content_msg.setText(stripHtml(msg));
                    } else {
                        UIUtils.showToastMsg(PrivacyActivity.this, jsonObject.optString("error"));
                        if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                            Intent intent = new Intent(PrivacyActivity.this, Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }
                } else {
                    UIUtils.showToast(PrivacyActivity.this, R.string.con_timeout);
                }

            }
        }.execute();


    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }

}

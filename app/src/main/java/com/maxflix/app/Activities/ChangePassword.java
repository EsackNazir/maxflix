package com.maxflix.app.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.maxflix.app.R;
import com.maxflix.app.Utils.ConnectionHelper;
import com.maxflix.app.Utils.PostHelper;
import com.maxflix.app.Utils.SharedPref;
import com.maxflix.app.Utils.UIUtils;
import com.maxflix.app.Utils.URLUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class ChangePassword extends AppCompatActivity {

    private ProgressBar progressBar;
    String user_id = "", user_token = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        Toolbar toolbar = (Toolbar) findViewById(R.id.forgot_in_toolbar);
        assert toolbar != null;
        toolbar.setTitle(R.string.ch_password);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        /*--------edit field-----------*/
        final EditText current_pass, new_pass, confirm_pass;
        current_pass = (EditText) findViewById(R.id.ed_current_pass);
        new_pass = (EditText) findViewById(R.id.ed_new_pass);
        confirm_pass = (EditText) findViewById(R.id.ed_confirm_pass);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);

        user_id = SharedPref.getKey(ChangePassword.this, "ID");
        user_token = SharedPref.getKey(ChangePassword.this, "TOKEN");

        /*--------lay edit field-----------*/
        final TextInputLayout current_lay, new_lay, confirm_lay;
        current_lay = (TextInputLayout) findViewById(R.id.lay_current_pass);
        new_lay = (TextInputLayout) findViewById(R.id.lay_new_pass);
        confirm_lay = (TextInputLayout) findViewById(R.id.lay_confirm_pass);

        Button change_btn = (Button) findViewById(R.id.change_pass);
        change_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Boolean checkField = false;
                final String str_current_pass, str_new_pass, str_confirm_pass;
                str_current_pass = current_pass.getText().toString();
                str_new_pass = new_pass.getText().toString();
                str_confirm_pass = confirm_pass.getText().toString();
                if (ConnectionHelper.isConnectingToInternet(ChangePassword.this)) {

                    if (current_pass.getText().toString().matches("")) {
                        current_lay.setError("Required Field");
                        checkField = true;
                    }
                    if (new_pass.getText().toString().matches("")) {
                        new_lay.setError("Required Field");
                        checkField = true;
                    }
                    if (confirm_pass.getText().toString().matches("")) {
                        confirm_lay.setError("Required Field");
                        checkField = true;
                    }
                    if (!checkField) {
                        if (str_new_pass.equalsIgnoreCase(str_confirm_pass)) {

                            new AsyncTask<JSONObject, JSONObject, JSONObject>() {

                                @Override
                                protected void onPreExecute() {
                                    super.onPreExecute();
                                    progressBar.setVisibility(View.VISIBLE);
                                }

                                @Override
                                protected JSONObject doInBackground(JSONObject... jsonObjects) {
                                    try {
                                        JSONObject object = new JSONObject();
                                        object.put("id", user_id);
                                        object.put("token", user_token);
                                        object.put("password", str_new_pass);
                                        object.put("old_password", str_current_pass);
                                        object.put("password_confirmation", str_confirm_pass);
                                        Log.e("Change_password", "" + object.toString());

                                        PostHelper postHelper = new PostHelper(ChangePassword.this);
                                        return postHelper.Post(URLUtils.change_Password, object.toString());

                                    } catch (JSONException | IOException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(JSONObject jsonObject) {
                                    super.onPostExecute(jsonObject);
                                    Log.e("ch_pass_res", "" + jsonObject);
                                    progressBar.setVisibility(View.GONE);
                                    if (jsonObject != null) {
                                        String result = jsonObject.optString("success");
                                        if (result.equalsIgnoreCase("true")) {
                                            UIUtils.showToastMsg(ChangePassword.this, jsonObject.optString("message"));
                                            Intent intent = new Intent(ChangePassword.this, MainActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                        } else {
                                            UIUtils.showToastMsg(ChangePassword.this, jsonObject.optString("error"));
                                            if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                                                Intent intent = new Intent(ChangePassword.this, Login.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                startActivity(intent);
                                                finish();
                                            }
                                        }
                                    } else {
                                        UIUtils.showToast(ChangePassword.this, R.string.con_timeout);
                                    }
                                }
                            }.execute();


                        } else {
                            UIUtils.showToastMsg(ChangePassword.this, "Password doesn't match");
                        }
                    }

                } else {
                    UIUtils.showNetworkAlert(ChangePassword.this, R.string.check_network);
                }

            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
        ConnectionHelper.isConnectingToInternet(ChangePassword.this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }
}

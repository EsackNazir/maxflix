package com.maxflix.app.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.maxflix.app.FullVideoActivity;
import com.maxflix.app.R;
import com.maxflix.app.Utils.CustomDialog;
import com.maxflix.app.Utils.PostHelper;
import com.maxflix.app.Utils.SharedPref;
import com.maxflix.app.Utils.UIUtils;
import com.maxflix.app.Utils.URLUtils;
import com.maxflix.app.models.CommentsItemModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tcking.github.com.giraffeplayer.GiraffePlayer;
import tv.danmaku.ijk.media.player.IMediaPlayer;

public class SingleVideoPage extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    private JSONObject video;
    private ScrollView fullLayout;
    private int rating_value;

    private String videoID = "", trailerVideo = "", image = "", youtube_video_id = "", shareUrl = "", wish_status = "";
    private TextView videoName, videoDes, add_wishList, comment, share;
    private RatingBar videoRating;
    private ProgressBar progressBar;
    private ImageView play_btn;
    private Button play_full_video;
    private ImageView back_button;

    GiraffePlayer player;
    YouTubePlayerView youTubePlayer;
    private String API = "AIzaSyAw1nNVn8q5OShMOsfUWDHR9ESOLLDkZas";
    private RelativeLayout griffe_player, youtube_player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_video_page);

        /*Toolbar toolbar = (Toolbar) findViewById(R.id.forgot_in_toolbar);
        assert toolbar != null;
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);*/

        /*toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });*/

        Intent intent = getIntent();
        videoID = intent.getStringExtra("videoID");
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        play_btn = (ImageView) findViewById(R.id.play_btn);
        play_btn.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        videoRating = (RatingBar) findViewById(R.id.videoRatings);
        share = (TextView) findViewById(R.id.share_text);
        play_full_video = (Button) findViewById(R.id.full_video);

        back_button = (ImageView) findViewById(R.id.back_arrow);
        back_button.setVisibility(View.GONE);

        loadingComment();

        griffe_player = (RelativeLayout) findViewById(R.id.giraffe_player_layout);
        griffe_player.setVisibility(View.GONE);
        youtube_player = (RelativeLayout) findViewById(R.id.youtube_player_layout);
        youtube_player.setVisibility(View.GONE);


        fullLayout = (ScrollView) findViewById(R.id.full_layout);
        fullLayout.setVisibility(View.GONE);
        videoName = (TextView) findViewById(R.id.video_headLine);
        videoDes = (TextView) findViewById(R.id.des);
        add_wishList = (TextView) findViewById(R.id.add_wishList);
        comment = (TextView) findViewById(R.id.comment);
        comment.setVisibility(View.GONE);

        //LinearLayout buffer_view = (LinearLayout) findViewById(R.id.buffer_layout);


        add_wishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String addWishList = "wishList";
                postValue(addWishList, progressBar, "", wish_status);
            }
        });

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        FloatingActionButton comments = (FloatingActionButton) findViewById(R.id.post_comment);
        comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CustomDialog customDialog = new CustomDialog(SingleVideoPage.this);
                customDialog.setContentView(R.layout.comment_dialog);

               /* RatingBar give_rating = (RatingBar) customDialog.findViewById(R.id.videoRatings);
                give_rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {

                        rating_value = (int) v;
                        String rating = "Your rating : " + rating_value;
                        UIUtils.showToastMsg(SingleVideoPage.this, rating);
                    }
                });*/

                ImageView imageView = (ImageView) customDialog.findViewById(R.id.banner_dialog);
                Glide.with(SingleVideoPage.this).load(image).centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL).crossFade(50).error(R.drawable.ic_profile).into(imageView);

                final EditText editText = (EditText) customDialog.findViewById(R.id.postComment);
                Button comment_ok = (Button) customDialog.findViewById(R.id.ok);
                comment_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String edit_string = editText.getText().toString();
                        if (edit_string.length() > 0) {
                            String addWishList = "";
                            postValue(addWishList, progressBar, edit_string,"");
                            customDialog.dismiss();
                        } else {
                            UIUtils.showToastMsg(SingleVideoPage.this, "Write the good comments.");
                        }


                    }
                });
                customDialog.show();
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, shareUrl);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);

            }
        });


    }

    private void addHistory(final String admin_video_id) {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(SingleVideoPage.this, "ID"));
                    jsonObject.put("token", SharedPref.getKey(SingleVideoPage.this, "TOKEN"));
                    jsonObject.put("admin_video_id", admin_video_id);

                    Log.e("addHistoryList", "" + jsonObject);
                    PostHelper postHelper = new PostHelper(SingleVideoPage.this);
                    return postHelper.Post(URLUtils.addHistory, jsonObject.toString());

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                progressBar.setVisibility(View.GONE);
                Log.e("addWishList", "" + jsonObject);
                if (jsonObject != null) {
                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {

                        //UIUtils.showToastMsg(SingleVideoPage.this, jsonObject.optString("message"));
                    } else {
                        UIUtils.showToastMsg(SingleVideoPage.this, jsonObject.optString("error"));
                        if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                            Intent intent = new Intent(SingleVideoPage.this, Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }
                } else {

                    UIUtils.showToast(SingleVideoPage.this, R.string.con_timeout);
                }

            }
        }.execute();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (player != null) {
            player.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (player != null) {
            player.onResume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (player != null) {
            player.onDestroy();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (player != null) {
            player.onConfigurationChanged(newConfig);
        }
    }

    @Override
    public void onBackPressed() {
        if (player != null && player.onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }

    private void   loadingComment() {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(SingleVideoPage.this, "ID"));
                    jsonObject.put("token", SharedPref.getKey(SingleVideoPage.this, "TOKEN"));
                    jsonObject.put("admin_video_id", videoID);

                    PostHelper postHelper = new PostHelper(SingleVideoPage.this);
                    return postHelper.Post(URLUtils.singleVideo, jsonObject.toString());
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                progressBar.setVisibility(View.GONE);
                Log.e("jsonObj", "" + jsonObject);
                if (jsonObject != null) {
                    fullLayout.setVisibility(View.VISIBLE);
                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {
                        video = jsonObject.optJSONObject("video");
                        shareUrl = jsonObject.optString("share_link");
                        String main_video = jsonObject.optString("main_video");
                        videoName.setText(video.optString("title"));
                        videoDes.setText(video.optString("description"));
                        image = video.optString("default_image");

                        float rating = Float.parseFloat(video.optString("ratings"));
                        videoRating.setRating(rating);
                        trailerVideo = video.optString("trailer_video");
                        if (jsonObject.optString("wishlist_status").equalsIgnoreCase("1")) {
                            add_wishList.setText(R.string.added_to_mylist);
                            wish_status = jsonObject.optString("wishlist_status");
                        } else if (jsonObject.optString("wishlist_status").equalsIgnoreCase("0")) {
                            add_wishList.setText(R.string.add_to_mylist);
                            wish_status = jsonObject.optString("wishlist_status");
                        }
                        if (video.optString("video_type").equalsIgnoreCase("2")) {
                            youtube_player.setVisibility(View.VISIBLE);
                            play_btn.setVisibility(View.GONE);
                            back_button.setVisibility(View.VISIBLE);
                            String youtube_video = video.optString("trailer_video");
                            String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

                            Pattern compiledPattern = Pattern.compile(pattern);
                            Matcher matcher = compiledPattern.matcher(youtube_video);

                            if (matcher.find()) {
                                youtube_video_id = matcher.group();
                                Log.e("youtube_video_id", "" + youtube_video_id);
                            }

                            youTubePlayer = (YouTubePlayerView) findViewById(R.id.youtube_view);
                            youTubePlayer.initialize(API, SingleVideoPage.this);

                            playFullVideo(jsonObject.optString("user_type"), play_full_video, main_video, video.optString("video_type"));

                            addHistory(video.optString("admin_video_id"));


                        } else if (video.optString("video_type").equalsIgnoreCase("1")) {
                            griffe_player.setVisibility(View.VISIBLE);
                            /*video player integration*/
                            player = new GiraffePlayer(SingleVideoPage.this);
                            play_btn.setVisibility(View.VISIBLE);
                            playFullVideo(jsonObject.optString("user_type"), play_full_video, main_video, video.optString("video_type"));

                            player.onComplete(new Runnable() {
                                @Override
                                public void run() {
                                    //callback when video is finish
                                    Toast.makeText(getApplicationContext(), "video play completed", Toast.LENGTH_SHORT).show();
                                }
                            }).onInfo(new GiraffePlayer.OnInfoListener() {
                                @Override
                                public void onInfo(int what, int extra) {
                                    switch (what) {
                                        case IMediaPlayer.MEDIA_INFO_BUFFERING_START:
                                            //do something when buffering start
                                            break;
                                        case IMediaPlayer.MEDIA_INFO_BUFFERING_END:
                                            //do something when buffering end
                                            break;
                                        case IMediaPlayer.MEDIA_INFO_NETWORK_BANDWIDTH:
                                            //download speed
                                            //((TextView) findViewById(R.id.tv_speed)).setText(Formatter.formatFileSize(getApplicationContext(),extra)+"/s");
                                            break;
                                        case IMediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START:
                                            //do something when video rendering
                                            //findViewById(R.id.tv_speed).setVisibility(View.GONE);
                                            break;
                                    }
                                }
                            }).onError(new GiraffePlayer.OnErrorListener() {
                                @Override
                                public void onError(int what, int extra) {
                                    Toast.makeText(getApplicationContext(), "video play error", Toast.LENGTH_SHORT).show();
                                }
                            });

                            play_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    player.play(trailerVideo);
                                    player.setTitle(videoName.getText().toString());
                                    play_btn.setVisibility(View.GONE);
                                    addHistory(video.optString("admin_video_id"));


                                }
                            });
                        }

                        JSONArray comment_array = jsonObject.optJSONArray("comments");
                        if (comment_array != null) {
                            if (comment_array.length() > 0)
                                comment.setVisibility(View.VISIBLE);
                            ArrayList<CommentsItemModel> commentsItemModels = new ArrayList<>();
                            for (int i = 0; i < comment_array.length(); i++) {
                                JSONObject object = comment_array.optJSONObject(i);
                                String userID = object.optString("user_id");
                                String username = object.optString("username");
                                String user_picture = object.optString("user_picture");
                                String com_rating = object.optString("rating");
                                String comment = object.optString("comment");
                                commentsItemModels.add(new CommentsItemModel(userID, username, user_picture, com_rating, comment));
                            }
                            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.comment_list);
                            recyclerView.setHasFixedSize(true);
                            recyclerView.setLayoutManager(new LinearLayoutManager(SingleVideoPage.this));
                            CommentAdapter commentAdapter = new CommentAdapter(commentsItemModels);
                            recyclerView.setAdapter(commentAdapter);
                        }

                    } else {
                        UIUtils.showToastMsg(SingleVideoPage.this, jsonObject.optString("error"));
                        if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                            Intent intent = new Intent(SingleVideoPage.this, Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }
                } else {
                    UIUtils.showToast(SingleVideoPage.this, R.string.con_timeout);
                }

            }
        }.execute();

    }

    private void playFullVideo(final String user_type, Button play_full_video, final String main_video, final String type) {
        play_full_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (user_type.equalsIgnoreCase("0")) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(SingleVideoPage.this);
                    builder.setMessage("Your are not paid user, if you paid after you can watch full video.");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialogInterface, int i) {
                            Intent broswer = new Intent(Intent.ACTION_VIEW);
                            broswer.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            broswer.setData(Uri.parse(URLUtils.base));
                            startActivity(broswer);
                            dialogInterface.dismiss();

                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                        }
                    });
                    builder.show();
                } else {

                    if (type.equalsIgnoreCase("2")) {
                        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
                        String videoURL = "";
                        Pattern compiledPattern = Pattern.compile(pattern);
                        Matcher matcher = compiledPattern.matcher(main_video);

                        if (matcher.find()) {
                            videoURL = matcher.group();
                            Log.e("MainVideo", "" + videoURL);

                            Intent video = new Intent(SingleVideoPage.this, FullVideoActivity.class);
                            video.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            video.putExtra("video_type", type);
                            video.putExtra("video_URL", videoURL);
                            startActivity(video);
                        }
                    } else {
                        Intent video = new Intent(SingleVideoPage.this, FullVideoActivity.class);
                        video.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        video.putExtra("video_type", type);
                        video.putExtra("video_URL", main_video);
                        startActivity(video);
                    }

                }

            }
        });

    }


    private void postValue(final String addWishList, final ProgressBar progressBar, final String comment, final String status) {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(SingleVideoPage.this, "ID"));
                    jsonObject.put("token", SharedPref.getKey(SingleVideoPage.this, "TOKEN"));
                    jsonObject.put("admin_video_id", video.optString("admin_video_id"));
                    if (addWishList.equalsIgnoreCase("wishList")) {
                        jsonObject.put("status", status);
                        Log.e("addWishList", "" + jsonObject);
                        PostHelper postHelper = new PostHelper(SingleVideoPage.this);
                        return postHelper.Post(URLUtils.addWishlist, jsonObject.toString());
                    } else {
                        jsonObject.put("rating", rating_value);
                        jsonObject.put("comments", comment);
                        Log.e("rating_comment", "" + jsonObject);
                        PostHelper postHelper = new PostHelper(SingleVideoPage.this);
                        return postHelper.Post(URLUtils.userRating, jsonObject.toString());
                    }

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                progressBar.setVisibility(View.GONE);
                Log.e("addWishList", "" + jsonObject);
                if (jsonObject != null) {
                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {
                        if (addWishList.equalsIgnoreCase("wishList") && jsonObject.optString("wishlist_status").equalsIgnoreCase("0")) {
                            add_wishList.setText(R.string.add_to_mylist);
                            wish_status =jsonObject.optString("wishlist_status");
                        } else if (addWishList.equalsIgnoreCase("wishList") && jsonObject.optString("wishlist_status").equalsIgnoreCase("1")) {
                            add_wishList.setText(R.string.added_to_mylist);
                            wish_status =jsonObject.optString("wishlist_status");
                        } else if (addWishList.isEmpty()) {
                            loadingComment();
                        }

                        UIUtils.showToastMsg(SingleVideoPage.this, jsonObject.optString("message"));
                    } else {
                        UIUtils.showToastMsg(SingleVideoPage.this, jsonObject.optString("error"));
                        if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                            Intent intent = new Intent(SingleVideoPage.this, Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }
                } else {

                    UIUtils.showToast(SingleVideoPage.this, R.string.con_timeout);
                }

            }
        }.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(API, this);
        }
    }

    private YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.youtube_view);
    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        youTubePlayer.cueVideo(youtube_video_id);

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Failured to Initialize!", Toast.LENGTH_LONG).show();
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, 1).show();
        } else {
            @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) String errorMessage = String.format(
                    getString(R.string.error_player), youTubeInitializationResult.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }


    private class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.MyViewHolder> {
        ArrayList<CommentsItemModel> itemModels;

        public CommentAdapter(ArrayList<CommentsItemModel> commentsItemModels) {
            this.itemModels = commentsItemModels;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(SingleVideoPage.this).inflate(R.layout.comment_list_item, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            Glide.with(SingleVideoPage.this).load(itemModels.get(position).getUser_picture()).diskCacheStrategy(DiskCacheStrategy.ALL).crossFade(50).error(R.drawable.ic_profile).into(holder.imageView);
            holder.rating_des.setText(itemModels.get(position).getComment());
            holder.user_name.setText(itemModels.get(position).getUsername());
            // holder.ratingBar.setRating(Float.parseFloat(itemModels.get(position).getRating()));
        }

        @Override
        public int getItemCount() {
            return itemModels.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            CircularImageView imageView;
            TextView user_name, rating_des;
            //RatingBar ratingBar;

            public MyViewHolder(View itemView) {
                super(itemView);
                imageView = (CircularImageView) itemView.findViewById(R.id.comment_image);
                user_name = (TextView) itemView.findViewById(R.id.comment_user_name);
                rating_des = (TextView) itemView.findViewById(R.id.comment_des);
                //ratingBar = (RatingBar) itemView.findViewById(R.id.comment_rating);
            }
        }
    }
}

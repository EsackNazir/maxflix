package com.maxflix.app.Activities;

import android.content.Intent;
import android.content.IntentSender;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.maxflix.app.R;
import com.maxflix.app.Utils.ConnectionHelper;
import com.maxflix.app.Utils.PostHelper;
import com.maxflix.app.Utils.SharedPref;
import com.maxflix.app.Utils.SingleTon;
import com.maxflix.app.Utils.UIUtils;
import com.maxflix.app.Utils.URLUtils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Register extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private ConnectionHelper connectionHelper;

    /*----------Facebook Login---------------*/
    private CallbackManager callbackManager;

    CircularImageView profile_image;
    String path = "";
    MultipartEntity entityBuilder;

    /*Google*/
    private static final int RC_SIGN_IN = 0;
    private GoogleApiClient mGoogleApiClient;
    private boolean mIntentInProgress;
    private boolean mSignInClicked;
    private ConnectionResult mConnectionResult;

    private ProgressBar progressBar;
    TextInputLayout lay_f_name, lay_l_name, lay_email, lay_phone, lay_password;
    EditText edt_f_name, edt_email, edt_phone, edt_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(Register.this);
        AppEventsLogger.activateApp(Register.this);
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_register);

        Toolbar toolbar = (Toolbar) findViewById(R.id.register_toolbar);
        toolbar.setTitle(R.string.register);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT > 15) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

          /*-------------Google Integration----------------- */
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addScope(new Scope(Scopes.PROFILE))
                .addScope(new Scope(Scopes.EMAIL))
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();


        edt_f_name = (EditText) findViewById(R.id.ed_first_name);
        edt_email = (EditText) findViewById(R.id.ed_email);
        edt_phone = (EditText) findViewById(R.id.ed_phone);
        edt_password = (EditText) findViewById(R.id.ed_password);

        lay_f_name = (TextInputLayout) findViewById(R.id.layout_f_name);
        lay_email = (TextInputLayout) findViewById(R.id.layout_email);
        lay_phone = (TextInputLayout) findViewById(R.id.layout_phone);
        lay_password = (TextInputLayout) findViewById(R.id.layout_password);

        profile_image = (CircularImageView) findViewById(R.id.profile_image);
        assert profile_image != null;
        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select Photo"), 1);
            }
        });

        getGCMKey();

        Button btn_google = (Button) findViewById(R.id.btn_google_sign);
        btn_google.setOnClickListener(this);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);

        Button register_btn = (Button) findViewById(R.id.register);
        register_btn.setOnClickListener(this);

        /*Facebook*/
        LoginButton fb_LoginButton = (LoginButton) findViewById(R.id.btn_facebook);
        assert fb_LoginButton != null;
        fb_LoginButton.setReadPermissions("public_profile email");
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        if (AccessToken.getCurrentAccessToken() != null) {
                            progressBar.setVisibility(View.VISIBLE);
                            GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(JSONObject object, GraphResponse response) {

                                    Log.d("object", "" + object);
                                    try {

                                        JSONObject fb_object = new JSONObject();
                                        String fb_id = object.getString("id");
                                        Log.e("fb_id", "" + fb_id);
                                        URL fb_profile_image = new URL("https://graph.facebook.com/" + fb_id + "/picture?type=large");
                                        String profile_img = fb_profile_image.toString();
                                        Log.d("fb_profile_image", "" + fb_profile_image.toString());

                                        fb_object.put("device_token", SingleTon.getInstance().GCMKey);
                                        fb_object.put("device_type", "android");
                                        fb_object.put("login_by", "facebook");
                                        fb_object.put("name", object.getString("name"));
                                        fb_object.put("mobile", "");
                                        fb_object.put("password", "");
                                        fb_object.put("picture", profile_img);
                                        fb_object.put("email", object.getString("email"));
                                        fb_object.put("social_unique_id", fb_id);
                                        Log.e("face_book_post_value", "" + fb_object);

                                        PostHelper postHelper = new PostHelper(Register.this);
                                        JSONObject lg_fb_post = postHelper.Post(URLUtils.register, fb_object.toString());

                                        Log.e("fb_object", "" + lg_fb_post);
                                        if (lg_fb_post != null) {

                                            String result = lg_fb_post.optString("success");
                                            if (result.equalsIgnoreCase("true")) {
                                                Intent mainIntent = new Intent(Register.this, MainActivity.class);
                                                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                SharedPref.putKey(Register.this, "login_type", "Logged");
                                                SharedPref.putKey(Register.this, "ID", object.optString("id"));
                                                SharedPref.putKey(Register.this, "PROFILE_IMAGE", object.optString("picture"));
                                                SharedPref.putKey(Register.this, "NAME", object.optString("name"));
                                                SharedPref.putKey(Register.this, "TOKEN", object.optString("token"));
                                                SharedPref.putKey(Register.this, "user_type", object.optString("user_type"));
                                                SharedPref.putKey(Register.this, "CHANGE_PASS", "Hide");
                                                startActivity(mainIntent);
                                                Register.this.finish();
                                            } else {
                                                UIUtils.showToastMsg(Register.this, lg_fb_post.optString("error"));
                                            }

                                        } else {
                                            UIUtils.showToast(Register.this, R.string.con_timeout);

                                        }
                                        progressBar.setVisibility(View.GONE);


                                    } catch (JSONException | IOException e) {
                                        e.printStackTrace();
                                    }

                                }

                            });
                            Bundle parameters = new Bundle();
                            parameters.putString("fields", "id,name,link,email,picture");
                            request.setParameters(parameters);
                            request.executeAsync();


                        }
                    }

                    @Override
                    public void onCancel() {
                        UIUtils.showToastMsg(Register.this, "Facebook login cancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        //UIUtils.showToast(Login.this, error.getMessage());
                    }
                }

        );


    }

    private void getGCMKey() {
         if (ConnectionHelper.isConnectingToInternet(Register.this))
            new AsyncTask<String, String, String>() {
                @Override
                protected String doInBackground(String... params) {

                    if (checkPlayServices()) {
                        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(Register.this);
                        try {
                            if (gcm == null) {
                                gcm = GoogleCloudMessaging.getInstance(Register.this);
                            }
                            //noinspection deprecation
                            SingleTon.getInstance().GCMKey = gcm.register(getResources().getString(R.string.GCM_SENDER));
                            Log.e("RegistrationID", "" + SingleTon.getInstance().GCMKey);

                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                    }

                    return null;
                }
            }.execute();
        else UIUtils.showToast(Register.this, R.string.check_network);
    }


    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            if (resultCode != RESULT_OK) {
                mSignInClicked = false;
            }
            mIntentInProgress = false;
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }
        if (resultCode == RESULT_OK && data != null && requestCode == 1) {
            Uri filePath = data.getData();
            Cursor cursor = null;
            try {
                String[] proj = {MediaStore.Images.Media.DATA};
                cursor = Register.this.getContentResolver().query(filePath, proj, null, null, null);
                assert cursor != null;
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                path = cursor.getString(column_index);
                Log.e("path", "" + path);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                profile_image.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        //noinspection deprecation
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        AppEventsLogger.activateApp(this);

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, 9000)
                        .show();
            } else {
                Log.i("MyDevice", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ConnectionHelper.isConnectingToInternet(Register.this)) {
            mSignInClicked = false;
            handleSignResult(true);
        } else {
            UIUtils.showNetworkAlert(Register.this, R.string.check_network);
        }
    }


    private void handleSignResult(final boolean result) {
        if (ConnectionHelper.isConnectingToInternet(Register.this))
            new AsyncTask<JSONObject, JSONObject, JSONObject>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progressBar.setVisibility(ProgressBar.VISIBLE);
                }

                @Override
                protected JSONObject doInBackground(JSONObject... params) {
                    if (result) {
                        //noinspection deprecation
                        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                            //noinspection deprecation
                            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                            String g_profile_name = currentPerson.getDisplayName();
                            String g_profile_ID = currentPerson.getId();
                            //noinspection deprecation
                            String g_profile_email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                            String str_profile_URL = currentPerson.getImage().getUrl();

                            Log.e("g_name", "" + g_profile_name);
                            Log.e("g_profile_ID", "" + g_profile_ID);
                            Log.e("g_profile_email", "" + g_profile_email);
                            Log.e("str_profile_URL", "" + str_profile_URL);

                            JSONObject object = new JSONObject();
                            try {
                                object.put("device_token", SingleTon.getInstance().GCMKey);
                                object.put("device_type", "android");
                                object.put("login_by", "google");
                                object.put("email", g_profile_email);
                                object.put("password", "");
                                object.put("mobile", "");
                                object.put("picture", str_profile_URL);
                                object.put("social_unique_id", g_profile_ID);
                                object.put("name", g_profile_name);


                                Log.e("google_post_value", "" + object);

                                PostHelper postHelper = new PostHelper(Register.this);
                                return postHelper.Post(URLUtils.register, object.toString());

                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(JSONObject object) {
                    super.onPostExecute(object);
                    progressBar.setVisibility(ProgressBar.GONE);
                    Log.e("Object", "" + object);
                    if (object != null) {
                        String result = object.optString("success");
                        if (result.equalsIgnoreCase("true")) {
                            Intent mainIntent = new Intent(Register.this, MainActivity.class);
                            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            SharedPref.putKey(Register.this, "login_type", "Logged");
                            SharedPref.putKey(Register.this, "ID", object.optString("id"));
                            SharedPref.putKey(Register.this, "PROFILE_IMAGE", object.optString("picture"));
                            SharedPref.putKey(Register.this, "NAME", object.optString("name"));
                            SharedPref.putKey(Register.this, "TOKEN", object.optString("token"));
                            SharedPref.putKey(Register.this, "user_type", object.optString("user_type"));
                            SharedPref.putKey(Register.this, "CHANGE_PASS", "Hide");
                            startActivity(mainIntent);
                            Register.this.finish();

                        } else {
                            UIUtils.showToastMsg(Register.this, object.optString("error"));

                        }
                    } else {
                        UIUtils.showToast(Register.this, R.string.con_timeout);
                    }

                }
            }.execute();
        else UIUtils.showNetworkAlert(Register.this, R.string.check_network);

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
        handleSignResult(false);
    }

    private void signInWithGplus() {
        if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInError();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.btn_google_sign:
                if (ConnectionHelper.isConnectingToInternet(Register.this)) {
                    progressBar.setVisibility(ProgressBar.VISIBLE);
                    signInWithGplus();
                } else {
                    UIUtils.showNetworkAlert(Register.this, R.string.check_network);
                }
                break;

            case R.id.register:
                if (ConnectionHelper.isConnectingToInternet(Register.this)) {
                    Boolean flag = false;
                    final String str_email, str_pass, str_fName, str_phone;

                    str_email = edt_email.getText().toString();
                    str_pass = edt_password.getText().toString();
                    str_fName = edt_f_name.getText().toString();
                    str_phone = edt_phone.getText().toString();

                    if ((!isValidEmail(str_email))) {
                        assert lay_email != null;
                        lay_email.setError("Required Field");
                        flag = true;
                    }
                    if (str_pass.length() <= 0) {
                        lay_password.setError("Required Field");
                        flag = true;
                    }
                    if (str_fName.length() <= 0) {
                        lay_f_name.setError("Required Field");
                        flag = true;
                    }

                    if (str_phone.length() <= 0) {
                        lay_phone.setError("Required Field");
                        flag = true;
                    }
                    if (!flag) {
                        new AsyncTask<String, Void, String>() {

                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                progressBar.setVisibility(View.VISIBLE);
                            }

                            @SuppressWarnings("deprecation")
                            @Override
                            protected String doInBackground(String... params) {
                                try {
                                    HttpClient client = new DefaultHttpClient();
                                    HttpPost httpPost = new HttpPost(URLUtils.register);
                                    entityBuilder = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

                                    if (path.isEmpty()) {
                                        Log.e("doInBack_file", ": is empty");
                                    } else {
                                        File file = new File(path);
                                        Log.e("doInBack_file", "" + file);
                                        entityBuilder.addPart("picture", new FileBody(file, "application/octet"));

                                        Log.d("EDIT USER PROFILE", "UPLOAD: file length = " + file.length());
                                        Log.d("EDIT USER PROFILE", "UPLOAD: file exist = " + file.exists());

                                    }

                                    entityBuilder.addPart("device_token", new StringBody(SingleTon.getInstance().GCMKey));
                                    entityBuilder.addPart("device_type", new StringBody("android"));
                                    entityBuilder.addPart("login_by", new StringBody("manual"));
                                    entityBuilder.addPart("name", new StringBody(str_fName));
                                    entityBuilder.addPart("email", new StringBody(str_email));
                                    entityBuilder.addPart("mobile", new StringBody(str_phone));
                                    entityBuilder.addPart("password", new StringBody(str_pass));
                                    entityBuilder.addPart("social_unique_id", new StringBody(""));


                                    httpPost.setEntity(entityBuilder);
                                    HttpResponse response = client.execute(httpPost);
                                    HttpEntity httpEntity = response.getEntity();

                                    if (response.getStatusLine().getStatusCode() == 200) {
                                        String res = EntityUtils.toString(httpEntity);
                                        Log.e("res", "" + res);
                                        return res;
                                    } else {
                                        return "Error";
                                    }

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                return null;
                            }

                            @Override
                            protected void onPostExecute(String s) {
                                super.onPostExecute(s);
                                progressBar.setVisibility(View.GONE);

                                if(s != null) {
                                    if (s.equalsIgnoreCase("Error")) {
                                        UIUtils.showToast(Register.this, R.string.con_timeout);
                                    } else {
                                        try {
                                            JSONObject object = new JSONObject(s);
                                            Log.i("object", "" + object);
                                            String result = object.optString("success");
                                            if (result.equalsIgnoreCase("true")) {
                                                Intent mainIntent = new Intent(Register.this, MainActivity.class);
                                                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                SharedPref.putKey(Register.this, "login_type", "Logged");
                                                SharedPref.putKey(Register.this, "ID", object.optString("id"));
                                                SharedPref.putKey(Register.this, "PROFILE_IMAGE", object.optString("picture"));
                                                SharedPref.putKey(Register.this, "NAME", object.optString("name"));
                                                SharedPref.putKey(Register.this, "TOKEN", object.optString("token"));
                                                SharedPref.putKey(Register.this, "user_type", object.optString("user_type"));
                                                SharedPref.putKey(Register.this, "CHANGE_PASS", "");
                                                startActivity(mainIntent);
                                                Register.this.finish();
                                            } else {
                                                UIUtils.showToastMsg(Register.this, object.optString("error_messages"));
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }.execute();
                    }
                } else {
                    UIUtils.showNetworkAlert(Register.this, R.string.check_network);
                }
                break;
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("Google_sign", "onConnectionFailed:" + connectionResult);
        progressBar.setVisibility(ProgressBar.GONE);
        if (!connectionResult.hasResolution()) {
            //noinspection deprecation
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this, 0).show();
            return;
        }
        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            mConnectionResult = connectionResult;

            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to
                // resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }
        //UIUtils.showToast(Register.this, R.string.google_Login_cancel);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ConnectionHelper.isConnectingToInternet(Register.this);
        //getGCMKey();
    }

    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}

package com.maxflix.app.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.vision.text.Line;
import com.maxflix.app.R;
import com.maxflix.app.Utils.PostHelper;
import com.maxflix.app.Utils.SharedPref;
import com.maxflix.app.Utils.UIUtils;
import com.maxflix.app.Utils.URLUtils;
import com.maxflix.app.models.SingleItemModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class MoreVideos extends AppCompatActivity {
    private RelativeLayout relativeLayout;
    private ArrayList<SingleItemModel> singleItemModels = new ArrayList<>();
    private RecyclerView recyclerView;
    private MoreVideoAdapter moreVideoAdapter;
    private int skip_value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_videos);
        Intent intent = getIntent();
        if (intent != null) {
            final String key = intent.getStringExtra("key");
            final String sectionName = intent.getStringExtra("sectionName");
            final String subCate = intent.getStringExtra("sub_cate");
            skip_value = 0;
            Toolbar toolbar = (Toolbar) findViewById(R.id.forgot_in_toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setTitle(sectionName);
            setSupportActionBar(toolbar);

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

            recyclerView = (RecyclerView) findViewById(R.id.more_recycler_list);
            final ProgressBar progressBar = (ProgressBar) findViewById(R.id.more_progress_bar);
            progressBar.setVisibility(View.GONE);

            relativeLayout = (RelativeLayout) findViewById(R.id.error_layout);
            relativeLayout.setVisibility(View.GONE);

            loadData(progressBar, subCate, key);
            recyclerView.setLayoutManager(new GridLayoutManager(MoreVideos.this, 3));
            moreVideoAdapter = new MoreVideoAdapter();
            recyclerView.setAdapter(moreVideoAdapter);

            moreVideoAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    Log.e("haint", "Load More");
                    singleItemModels.add(null);
                    moreVideoAdapter.notifyItemInserted(singleItemModels.size() - 1);
                    //Load data
                    int index = singleItemModels.size();
                    skipLoadData(progressBar, subCate, key, index);

                }
            });


        }

    }

    private void skipLoadData(final ProgressBar progressBar, final String subCate, final String key, final int skip_value) {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {

            /*@Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }*/
            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(MoreVideos.this, "ID"));
                    jsonObject.put("token", SharedPref.getKey(MoreVideos.this, "TOKEN"));

                    PostHelper postHelper = new PostHelper(MoreVideos.this);
                    if (subCate.equalsIgnoreCase("sub")) {
                        jsonObject.put("sub_category_id", key);
                        jsonObject.put("skip", skip_value);
                        Log.e("more_videos", "" + jsonObject);
                        return postHelper.Post(URLUtils.subCategoryVideos, jsonObject.toString());
                    } else {
                        jsonObject.put("key", key);
                        jsonObject.put("skip", skip_value);
                        Log.e("more_videos", "" + jsonObject);
                        return postHelper.Post(URLUtils.common, jsonObject.toString());
                    }

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                //progressBar.setVisibility(View.GONE);

                Log.e("more_skip_values", "" + jsonObject);
                if (jsonObject != null) {

                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {
                        JSONArray dataArray = jsonObject.optJSONArray("data");

                        //Remove loading item
                        singleItemModels.remove(singleItemModels.size() - 1);
                        moreVideoAdapter.notifyItemRemoved(singleItemModels.size());

                        if (dataArray.length()>0){
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject object = dataArray.optJSONObject(i);
                                String admin_video_id = object.optString("admin_video_id");
                                String publish_time = object.optString("publish_time");
                                String watch_count = object.optString("watch_count");
                                String title = object.optString("title");
                                String description = object.optString("description");
                                String default_image = object.optString("default_image");
                                String category_name = object.optString("category_name");
                                String wishlist_id = object.optString("wishlist_id");
                                singleItemModels.add(new SingleItemModel(admin_video_id, publish_time, watch_count, title, description, default_image, category_name, wishlist_id));
                            }
                            moreVideoAdapter.notifyDataSetChanged();
                            moreVideoAdapter.setLoaded();
                        }

                        if (singleItemModels.isEmpty()) {
                            relativeLayout.setVisibility(View.VISIBLE);
                        } else {

                        }


                    } else {
                        UIUtils.showToastMsg(MoreVideos.this, jsonObject.optString("error"));
                        if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                            Intent intent = new Intent(MoreVideos.this, Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }
                } else {
                    UIUtils.showToast(MoreVideos.this, R.string.con_timeout);
                    relativeLayout.setVisibility(View.VISIBLE);
                }

            }
        }.execute();
    }


    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView wishImage/*, wishOptional*/;
        TextView wishName /*wishTime, wishCate*/;

        public MyViewHolder(final View itemView) {
            super(itemView);
            wishImage = (ImageView) itemView.findViewById(R.id.wish_list_image);
            //wishOptional = (ImageView) itemView.findViewById(R.id.optional);
            wishName = (TextView) itemView.findViewById(R.id.wish_name);
            //wishTime = (TextView) itemView.findViewById(R.id.wish_time);
            //wishCate = (TextView) itemView.findViewById(R.id.wish_category);


        }
    }

    private void loadData(final ProgressBar progressBar, final String subCate, final String key) {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(MoreVideos.this, "ID"));
                    jsonObject.put("token", SharedPref.getKey(MoreVideos.this, "TOKEN"));


                    PostHelper postHelper = new PostHelper(MoreVideos.this);
                    if (subCate.equalsIgnoreCase("sub")) {
                        jsonObject.put("sub_category_id", key);
                        Log.e("more_videos", "" + jsonObject);
                        return postHelper.Post(URLUtils.subCategoryVideos, jsonObject.toString());
                    } else {
                        jsonObject.put("key", key);
                        jsonObject.put("skip", skip_value);
                        Log.e("more_videos", "" + jsonObject);
                        return postHelper.Post(URLUtils.common, jsonObject.toString());
                    }

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                progressBar.setVisibility(View.GONE);

                Log.e("more", "" + jsonObject);
                if (jsonObject != null) {
                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {
                        JSONArray dataArray = jsonObject.optJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject object = dataArray.optJSONObject(i);
                            String admin_video_id = object.optString("admin_video_id");
                            String publish_time = object.optString("publish_time");
                            String watch_count = object.optString("watch_count");
                            String title = object.optString("title");
                            String description = object.optString("description");
                            String default_image = object.optString("default_image");
                            String category_name = object.optString("category_name");
                            String wishlist_id = object.optString("wishlist_id");
                            singleItemModels.add(new SingleItemModel(admin_video_id, publish_time, watch_count, title, description, default_image, category_name, wishlist_id));
                        }
                        if (singleItemModels.isEmpty()) {
                            relativeLayout.setVisibility(View.VISIBLE);
                        } else {

                        }


                    } else {
                        UIUtils.showToastMsg(MoreVideos.this, jsonObject.optString("error"));
                        if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                            Intent intent = new Intent(MoreVideos.this, Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }
                } else {
                    UIUtils.showToast(MoreVideos.this, R.string.con_timeout);
                    relativeLayout.setVisibility(View.VISIBLE);
                }

            }
        }.execute();
    }

    private class MoreVideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        /*ArrayList<SingleItemModel> my_list;
        ProgressBar myProgressBar;*/

        private final int VIEW_TYPE_ITEM = 0;
        private final int VIEW_TYPE_LOADING = 1;

        private boolean isLoading;
        private int visibleThreshold = 5;
        private int lastVisibleItem, totalItemCount;

        private OnLoadMoreListener mOnLoadMoreListener;

        public MoreVideoAdapter() {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.onLoadMore();
                        }
                        isLoading = true;
                    }
                }
            });
        }


        public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
            this.mOnLoadMoreListener = mOnLoadMoreListener;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            if (viewType == VIEW_TYPE_ITEM) {
                View view = LayoutInflater.from(MoreVideos.this).inflate(R.layout.my_wish_list_item, parent, false);
                return new MyViewHolder(view);
            } else if (viewType == VIEW_TYPE_LOADING) {
                View view = LayoutInflater.from(MoreVideos.this).inflate(R.layout.progress_bar, parent, false);
                return new LoadingViewHolder(view);
            }
            return null;
        }


        @Override
        public int getItemViewType(int position) {
            return singleItemModels.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

            if (holder instanceof MyViewHolder) {
                MyViewHolder myViewHolder = (MyViewHolder) holder;
                Glide.with(MoreVideos.this).load(singleItemModels.get(position).getDefault_image()).centerCrop().error(R.drawable.ic_profile).crossFade(50).into(myViewHolder.wishImage);
                myViewHolder.wishName.setText(singleItemModels.get(position).getTitle());
                //holder.wishTime.setText(my_list.get(position).getPublish_time());
                //holder.wishCate.setText(my_list.get(position).getCategory_name());
                myViewHolder.wishImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String video_ID = singleItemModels.get(position).getAdmin_video_id();
                        Intent intent = new Intent(MoreVideos.this, SingleVideoPage.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("videoID", video_ID);
                        startActivity(intent);
                    }
                });
            } else if (holder instanceof LoadingViewHolder) {
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setIndeterminate(true);
            }


            /*holder.wishImage.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    final String video_ID = my_list.get(position).getAdmin_video_id();
                    PopupMenu popupMenu = new PopupMenu(MoreVideos.this, holder.wishImage);
                    popupMenu.getMenuInflater().inflate(R.menu.add__wishlist, popupMenu.getMenu());
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            new AsyncTask<JSONObject, JSONObject, JSONObject>() {

                                @Override
                                protected void onPreExecute() {
                                    super.onPreExecute();
                                    myProgressBar.setVisibility(View.VISIBLE);
                                }

                                @Override
                                protected JSONObject doInBackground(JSONObject... jsonObjects) {
                                    JSONObject jsonObject = new JSONObject();
                                    try {
                                        jsonObject.put("id", SharedPref.getKey(MoreVideos.this, "ID"));
                                        jsonObject.put("token", SharedPref.getKey(MoreVideos.this, "TOKEN"));
                                        jsonObject.put("admin_video_id", video_ID);
                                        jsonObject.put("status", "");
                                        Log.e("removePost", "" + jsonObject);
                                        PostHelper postHelper = new PostHelper(MoreVideos.this);
                                        return postHelper.Post(URLUtils.addWishlist, jsonObject.toString());
                                    } catch (JSONException | IOException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(JSONObject jsonObject) {
                                    super.onPostExecute(jsonObject);
                                    Log.e("add_wishList", "" + jsonObject);
                                    myProgressBar.setVisibility(View.GONE);
                                    if (jsonObject != null) {
                                        if (jsonObject.optString("success").equalsIgnoreCase("true")) {

                                        } else {
                                            UIUtils.showToastMsg(MoreVideos.this, jsonObject.optString("error"));
                                            if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                                                Intent intent = new Intent(MoreVideos.this, Login.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                startActivity(intent);
                                            }
                                        }
                                    } else {

                                        UIUtils.showToast(MoreVideos.this, R.string.con_timeout);
                                    }

                                }
                            }.execute();
                            return false;
                        }
                    });
                    popupMenu.show();
                    return false;
                }
            });*/

            /*holder.wishOptional.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });*/

        }

        @Override
        public int getItemCount() {
            return singleItemModels == null ? 0 : singleItemModels.size();
        }

        public void setLoaded() {
            isLoading = false;
        }

    }
}

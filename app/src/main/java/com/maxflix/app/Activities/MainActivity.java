package com.maxflix.app.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.maxflix.app.Fragment.CommonCateFragment;
import com.maxflix.app.Fragment.CommonFragment;
import com.maxflix.app.Fragment.History;
import com.maxflix.app.Fragment.MyList;
import com.maxflix.app.Fragment.Profile;
import com.maxflix.app.Fragment.SettingsFragment;
import com.maxflix.app.R;
import com.maxflix.app.Utils.ConnectionHelper;
import com.maxflix.app.Utils.ListUtils;
import com.maxflix.app.Utils.PostHelper;
import com.maxflix.app.Utils.SharedPref;
import com.maxflix.app.Utils.UIUtils;
import com.maxflix.app.Utils.URLUtils;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

@SuppressWarnings("deprecation")
public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, ResultCallback<People.LoadPeopleResult>, View.OnClickListener {
// test added



    GoogleApiClient mGoogleApiClient;
    public static FragmentManager fragmentManager;
    private boolean doubleBackToExitPressedOnce = false;

    private ImageView nav_home, nav_myList, nav_cate, nav_history, nav_setting, nav_logout;

    private ListView listView;
    private MainCateAdapter mainCateAdapter;
    private DrawerLayout drawer;
    boolean status = true;
    private String ID = "", TOKEN = "";
    Fragment fragment = null;
    FragmentManager manager = null;
    @SuppressLint("CommitTransaction")
    FragmentTransaction transaction = null;

    @SuppressLint("CommitTransaction")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(MainActivity.this);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ID = SharedPref.getKey(MainActivity.this, "ID");
        TOKEN = SharedPref.getKey(MainActivity.this, "TOKEN");

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(new Scope(Scopes.PROFILE))
                .addScope(new Scope(Scopes.EMAIL))
                .build();
        mGoogleApiClient.connect();


        LinearLayout home, myList, cate, history, setting, logout;
        home = (LinearLayout) findViewById(R.id.home);
        myList = (LinearLayout) findViewById(R.id.my_ist);
        cate = (LinearLayout) findViewById(R.id.category);
        history = (LinearLayout) findViewById(R.id.history);
        setting = (LinearLayout) findViewById(R.id.settings);
        logout = (LinearLayout) findViewById(R.id.logout);

        home.setOnClickListener(this);
        myList.setOnClickListener(this);
        cate.setOnClickListener(this);
        history.setOnClickListener(this);
        setting.setOnClickListener(this);
        logout.setOnClickListener(this);

        nav_home = (ImageView) findViewById(R.id.nav_home);
        nav_myList = (ImageView) findViewById(R.id.nav_my_list);
        nav_cate = (ImageView) findViewById(R.id.nav_category);
        nav_history = (ImageView) findViewById(R.id.nav_history);
        nav_setting = (ImageView) findViewById(R.id.nav_settings);
        nav_logout = (ImageView) findViewById(R.id.nav_logout);

        nav_home.setVisibility(View.GONE);
        nav_myList.setVisibility(View.GONE);
        nav_cate.setVisibility(View.GONE);
        nav_history.setVisibility(View.GONE);
        nav_setting.setVisibility(View.GONE);
        nav_logout.setVisibility(View.GONE);

        showNavigation("home");
        fragment = new CommonFragment();
        manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.content, fragment);
        transaction.commit();
        fragmentManager = getSupportFragmentManager();

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        TextView name = (TextView) navigationView.findViewById(R.id.profile_name);
        CircularImageView circularImageView = (CircularImageView) navigationView.findViewById(R.id.profile_image);
        circularImageView.setOnClickListener(this);
        name.setText(SharedPref.getKey(MainActivity.this, "NAME"));
        Glide.with(MainActivity.this).load(SharedPref.getKey(MainActivity.this, "PROFILE_IMAGE")).error(R.drawable.ic_profile).crossFade(50).centerCrop().into(circularImageView);
        listView = (ListView) navigationView.findViewById(R.id.cate_list);
        listView.setVisibility(View.GONE);
        drawer = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        assert drawer != null;
        //noinspection deprecation
        drawer.setDrawerListener(toggle);
        drawer.setStatusBarBackgroundColor(Color.WHITE);
        toggle.syncState();
        getMainCategory();


    }

    private void getMainCategory() {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {
            JSONArray category_array;


            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", ID);
                    jsonObject.put("token", TOKEN);
                    PostHelper postHelper = new PostHelper(MainActivity.this);
                    return postHelper.Post(URLUtils.categories, jsonObject.toString());
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                Log.e("cate", "" + jsonObject);
                if (jsonObject != null) {
                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {
                        category_array = jsonObject.optJSONArray("categories");
                        mainCateAdapter = new MainCateAdapter(category_array);
                        listView.setAdapter(mainCateAdapter);
                        ListUtils.getListViewSize(listView);
                    } else {
                        UIUtils.showToastMsg(MainActivity.this, jsonObject.optString("error"));
                        if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                            Intent intent = new Intent(MainActivity.this, Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }
                } else {
                    UIUtils.showToast(MainActivity.this, R.string.con_timeout);
                }
            }
        }.execute();
    }

    @SuppressLint("CommitTransaction")
    @Override
    public void onClick(View view) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        switch (view.getId()) {
            case R.id.profile_image:
                if (ConnectionHelper.isConnectingToInternet(MainActivity.this)) {
                    drawer.closeDrawer(GravityCompat.START);
                    toolbar.setTitle("Profile");
                    fragment = new Profile();
                    manager = getSupportFragmentManager();
                    transaction = manager.beginTransaction();
                    transaction.replace(R.id.content, fragment);
                    transaction.commit();
                    fragmentManager = getSupportFragmentManager();

                } else {
                    UIUtils.showNetworkAlert(MainActivity.this, R.string.check_network);
                }
                break;
            case R.id.home:
                if (ConnectionHelper.isConnectingToInternet(MainActivity.this)) {
                    showNavigation("home");
                    toolbar.setTitle("Home");
                    drawer.closeDrawer(GravityCompat.START);
                    fragment = new CommonFragment();
                    manager = getSupportFragmentManager();
                    transaction = manager.beginTransaction();
                    transaction.replace(R.id.content, fragment);
                    transaction.commit();
                    fragmentManager = getSupportFragmentManager();

                } else {
                    UIUtils.showNetworkAlert(MainActivity.this, R.string.check_network);
                }
                break;
            case R.id.my_ist:
                showNavigation("myList");
                toolbar.setTitle("My List");
                drawer.closeDrawer(GravityCompat.START);
                fragment = new MyList();
                manager = getSupportFragmentManager();
                transaction = manager.beginTransaction();
                transaction.replace(R.id.content, fragment);
                transaction.commit();
                fragmentManager = getSupportFragmentManager();
                break;
            case R.id.category:
                /*showNavigation("category");
                toolbar.setTitle("Category");
                drawer.closeDrawer(GravityCompat.START);
                fragment = new MainCategory();
                manager = getSupportFragmentManager();
                transaction = manager.beginTransaction();
                transaction.replace(R.id.content, fragment);
                transaction.commit();
                fragmentManager = getSupportFragmentManager();*/
                if (status) {
                    listView.setVisibility(View.VISIBLE);
                    status = false;
                } else {
                    listView.setVisibility(View.GONE);
                    status = true;
                }

                break;
            case R.id.history:
                showNavigation("history");
                toolbar.setTitle("History");
                drawer.closeDrawer(GravityCompat.START);
                fragment = new History();
                manager = getSupportFragmentManager();
                transaction = manager.beginTransaction();
                transaction.replace(R.id.content, fragment);
                transaction.commit();
                fragmentManager = getSupportFragmentManager();
                break;
            case R.id.settings:
                showNavigation("setting");
                toolbar.setTitle("Settings");
                drawer.closeDrawer(GravityCompat.START);
                fragment = new SettingsFragment();
                manager = getSupportFragmentManager();
                transaction = manager.beginTransaction();
                transaction.replace(R.id.content, fragment);
                transaction.commit();
                fragmentManager = getSupportFragmentManager();
                break;
            case R.id.logout:
                showNavigation("logout");
                drawer.closeDrawer(GravityCompat.START);
                if (ConnectionHelper.isConnectingToInternet(MainActivity.this)) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager loginManager = LoginManager.getInstance();
                        loginManager.logOut();
                        Intent logIntent = new Intent(MainActivity.this, Login.class);
                        logIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        SharedPref.putKey(MainActivity.this, "login_type", "");
                        startActivity(logIntent);
                        finish();

                    } else if (mGoogleApiClient.isConnected()) {
                        Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
                        mGoogleApiClient.disconnect();
                        Intent logIntent = new Intent(MainActivity.this, Login.class);
                        logIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        SharedPref.putKey(MainActivity.this, "login_type", "");
                        startActivity(logIntent);
                        finish();

                    } else if (SharedPref.getKey(MainActivity.this, "login_type").equalsIgnoreCase("Logged")) {
                        Intent logIntent = new Intent(MainActivity.this, Login.class);
                        logIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        SharedPref.putKey(MainActivity.this, "login_type", "");
                        startActivity(logIntent);
                        finish();

                    }
                } else {
                    UIUtils.showNetworkAlert(MainActivity.this, R.string.check_network);
                }
                break;
        }

    }

    private void showNavigation(String value) {
        if (value.equalsIgnoreCase("home")) {
            nav_home.setVisibility(View.VISIBLE);
            nav_myList.setVisibility(View.GONE);
            nav_cate.setVisibility(View.GONE);
            nav_history.setVisibility(View.GONE);
            nav_setting.setVisibility(View.GONE);
            nav_logout.setVisibility(View.GONE);
        } else if (value.equalsIgnoreCase("myList")) {
            nav_home.setVisibility(View.GONE);
            nav_myList.setVisibility(View.VISIBLE);
            nav_cate.setVisibility(View.GONE);
            nav_history.setVisibility(View.GONE);
            nav_setting.setVisibility(View.GONE);
            nav_logout.setVisibility(View.GONE);

        } else if (value.equalsIgnoreCase("category")) {
            nav_home.setVisibility(View.GONE);
            nav_myList.setVisibility(View.GONE);
            nav_cate.setVisibility(View.VISIBLE);
            nav_history.setVisibility(View.GONE);
            nav_setting.setVisibility(View.GONE);
            nav_logout.setVisibility(View.GONE);
        } else if (value.equalsIgnoreCase("history")) {
            nav_home.setVisibility(View.GONE);
            nav_myList.setVisibility(View.GONE);
            nav_cate.setVisibility(View.GONE);
            nav_history.setVisibility(View.VISIBLE);
            nav_setting.setVisibility(View.GONE);
            nav_logout.setVisibility(View.GONE);
        } else if (value.equalsIgnoreCase("setting")) {
            nav_home.setVisibility(View.GONE);
            nav_myList.setVisibility(View.GONE);
            nav_cate.setVisibility(View.GONE);
            nav_history.setVisibility(View.GONE);
            nav_setting.setVisibility(View.VISIBLE);
            nav_logout.setVisibility(View.GONE);
        } else if (value.equalsIgnoreCase("logout")) {
            nav_home.setVisibility(View.GONE);
            nav_myList.setVisibility(View.GONE);
            nav_cate.setVisibility(View.GONE);
            nav_history.setVisibility(View.GONE);
            nav_setting.setVisibility(View.GONE);
            nav_logout.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
                UIUtils.showToastMsg(MainActivity.this, "Press the back button once again to close the application.");
                doubleBackToExitPressedOnce = true;
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        Plus.PeopleApi.loadVisible(mGoogleApiClient, null).setResultCallback(this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull People.LoadPeopleResult loadPeopleResult) {

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        isFinishing();

    }

    @Override
    protected void onResume() {
        super.onResume();
        this.doubleBackToExitPressedOnce = false;
        ConnectionHelper.isConnectingToInternet(MainActivity.this);
    }

    private class MainCateAdapter extends BaseAdapter {
        JSONArray dataSet;

        public MainCateAdapter(JSONArray category_array) {
            this.dataSet = category_array;
        }

        @Override
        public int getCount() {
            return dataSet.length();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            @SuppressLint("ViewHolder") View myView = LayoutInflater.from(MainActivity.this).inflate(R.layout.nav_cate_list_item, viewGroup, false);
            TextView name = (TextView) myView.findViewById(R.id.nav_cate_name);
            name.setText(dataSet.optJSONObject(i).optString("name"));

            myView.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("CommitTransaction")
                @Override
                public void onClick(View view) {
                    showNavigation("category");
                    drawer.closeDrawer(GravityCompat.START);
                    String ID = dataSet.optJSONObject(i).optString("id");
                    Fragment fragment = new CommonCateFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("id", ID);
                    bundle.putString("sub_cate", "sub");
                    bundle.putString("name", dataSet.optJSONObject(i).optString("name"));
                    fragment.setArguments(bundle);
                    manager = getSupportFragmentManager();
                    transaction = manager.beginTransaction();
                    transaction.replace(R.id.content, fragment);
                    transaction.commit();
                    fragmentManager = getSupportFragmentManager();
                }
            });

            return myView;
        }
    }
}

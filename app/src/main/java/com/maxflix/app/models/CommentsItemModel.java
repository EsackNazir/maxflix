package com.maxflix.app.models;

/**
 * Created by Karthika on 8/28/2016.
 */
public class CommentsItemModel {
    private String user_id;
    private String username;
    private String user_picture;
    private String rating;
    private String comment;

    public CommentsItemModel(String user_id, String username, String user_picture, String rating, String comment) {
        this.user_id = user_id;
        this.username = username;
        this.user_picture = user_picture;
        this.rating = rating;
        this.comment = comment;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUser_picture() {
        return user_picture;
    }

    public void setUser_picture(String user_picture) {
        this.user_picture = user_picture;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}

package com.maxflix.app.models;

/**
 * Created by User on 22-09-2016.
 */

public class SearchList {

    public String id="";
    public String movie_name="";
    public String movie_image="";

    public SearchList(String id, String movie_name, String movie_image) {
        this.id = id;
        this.movie_name = movie_name;
        this.movie_image = movie_image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMovie_name() {
        return movie_name;
    }

    public void setMovie_name(String movie_name) {
        this.movie_name = movie_name;
    }

    public String getMovie_image() {
        return movie_image;
    }

    public void setMovie_image(String movie_image) {
        this.movie_image = movie_image;
    }
}

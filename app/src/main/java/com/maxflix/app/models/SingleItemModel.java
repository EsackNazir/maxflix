package com.maxflix.app.models;

/**
 * Created by pratap.kesaboyina on 01-12-2015.
 */
public class SingleItemModel {

    private String admin_video_id;
    private String publish_time;
    private String watch_count;
    private String title;
    private String description;
    private String default_image;
    private String category_name;
    private String wishlist_id;


    public SingleItemModel(String admin_video_id,
                           String publish_time, String watch_count, String title,
                           String description, String default_image, String category_name, String wishlist_id) {
        this.admin_video_id = admin_video_id;
        this.publish_time = publish_time;
        this.watch_count = watch_count;
        this.title = title;
        this.description = description;
        this.default_image = default_image;
        this.category_name = category_name;
        this.wishlist_id = wishlist_id;


    }

    public String getWishlist_id() {
        return wishlist_id;
    }

    public void setWishlist_id(String wishlist_id) {
        this.wishlist_id = wishlist_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getAdmin_video_id() {
        return admin_video_id;
    }

    public void setAdmin_video_id(String admin_video_id) {
        this.admin_video_id = admin_video_id;
    }

    public String getPublish_time() {
        return publish_time;
    }

    public void setPublish_time(String publish_time) {
        this.publish_time = publish_time;
    }

    public String getWatch_count() {
        return watch_count;
    }

    public void setWatch_count(String watch_count) {
        this.watch_count = watch_count;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDefault_image() {
        return default_image;
    }

    public void setDefault_image(String default_image) {
        this.default_image = default_image;
    }

}

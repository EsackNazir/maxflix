package com.maxflix.app.adapters;


import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.maxflix.app.Activities.MoreVideos;
import com.maxflix.app.R;
import com.maxflix.app.models.SectionDataModel;

import java.util.ArrayList;

public class RecyclerViewDataAdapter extends RecyclerView.Adapter<RecyclerViewDataAdapter.ItemRowHolder> {

    private ArrayList<SectionDataModel> dataList;
    private Context mContext;
    public ProgressBar progressBar;
    private String cate="";

    public RecyclerViewDataAdapter(Context context, ArrayList<SectionDataModel> dataList, String sub_cate) {
        this.dataList = dataList;
        this.mContext = context;
        this.cate = sub_cate;

    }

    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
        return new ItemRowHolder(v);
    }

    @Override
    public void onBindViewHolder(ItemRowHolder itemRowHolder, int i) {

        final String sectionName = dataList.get(i).getHeaderTitle();
        final String key = dataList.get(i).getKey();
        ArrayList singleSectionItems = dataList.get(i).getAllItemsInSection();
        SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(mContext, singleSectionItems, progressBar);

        if (singleSectionItems.isEmpty()) {
            itemRowHolder.itemTitle.setVisibility(View.GONE);
            itemRowHolder.btnMore.setVisibility(View.GONE);
            itemRowHolder.arrow.setVisibility(View.GONE);
        } else {
            itemRowHolder.itemTitle.setText(sectionName);
            itemRowHolder.recycler_view_list.setHasFixedSize(true);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            itemRowHolder.recycler_view_list.setLayoutManager(linearLayoutManager);
            itemRowHolder.recycler_view_list.setAdapter(itemListDataAdapter);

            //itemRowHolder.recycler_view_list.setNestedScrollingEnabled(false);


       /*     itemRowHolder.recycler_view_list.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    int action = event.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            // Disallow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            break;
                        case MotionEvent.ACTION_UP:
                            //Allow ScrollView to intercept touch events once again.
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                    // Handle RecyclerView touch events.
                    v.onTouchEvent(event);
                    return true;
                }
            });*/

            itemRowHolder.btnMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, MoreVideos.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("key", key);
                    intent.putExtra("sub_cate",cate);
                    intent.putExtra("sectionName", sectionName);
                    mContext.startActivity(intent);
                    //Toast.makeText(v.getContext(), "click event on more, " + sectionName, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView itemTitle;
        protected RecyclerView recycler_view_list;
        protected TextView btnMore;
        protected ImageView arrow;


        public ItemRowHolder(View view) {
            super(view);

            this.itemTitle = (TextView) view.findViewById(R.id.itemTitle);
            this.recycler_view_list = (RecyclerView) view.findViewById(R.id.recycler_view_list);
            this.btnMore = (TextView) view.findViewById(R.id.btnMore);
            this.arrow = (ImageView) view.findViewById(R.id.right_arrow);


        }

    }

}
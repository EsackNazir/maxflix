package com.maxflix.app.adapters;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.maxflix.app.Activities.Login;
import com.maxflix.app.Activities.SingleVideoPage;
import com.maxflix.app.R;
import com.maxflix.app.Utils.PostHelper;
import com.maxflix.app.Utils.SharedPref;
import com.maxflix.app.Utils.UIUtils;
import com.maxflix.app.Utils.URLUtils;
import com.maxflix.app.models.SingleItemModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class SectionListDataAdapter extends RecyclerView.Adapter<SectionListDataAdapter.SingleItemRowHolder> {

    private ArrayList<SingleItemModel> itemsList;
    private Context mContext;
    private ProgressBar progress;

    public SectionListDataAdapter(Context context, ArrayList<SingleItemModel> itemsList, ProgressBar progressBar) {
        this.itemsList = itemsList;
        this.mContext = context;
        this.progress = progressBar;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.common_list_item, viewGroup, false);
        return new SingleItemRowHolder(v);
    }

    @Override
    public void onBindViewHolder(final SingleItemRowHolder holder, int i) {

        final SingleItemModel singleItem = itemsList.get(i);

        holder.wishName.setText(singleItem.getTitle());
        //holder.wishTime.setText(singleItem.getPublish_time());
        //holder.wishCate.setText(singleItem.getCategory_name());
        Glide.with(mContext).load(singleItem.getDefault_image()).centerCrop().error(R.drawable.ic_profile).crossFade(50).into(holder.wishImage);

        holder.wishImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String video_ID = singleItem.getAdmin_video_id();
                Intent intent = new Intent(mContext, SingleVideoPage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("videoID", video_ID);
                mContext.startActivity(intent);
            }
        });

        /*holder.wishImage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                final String video_ID = singleItem.getAdmin_video_id();
                PopupMenu popupMenu = new PopupMenu(mContext, holder.wishImage);
                popupMenu.getMenuInflater().inflate(R.menu.add__wishlist, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        new AsyncTask<JSONObject, JSONObject, JSONObject>() {

                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                progress.setVisibility(View.VISIBLE);
                            }

                            @Override
                            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                                JSONObject jsonObject = new JSONObject();
                                try {
                                    jsonObject.put("id", SharedPref.getKey(mContext, "ID"));
                                    jsonObject.put("token", SharedPref.getKey(mContext, "TOKEN"));
                                    jsonObject.put("admin_video_id", video_ID);
                                    jsonObject.put("status", "");
                                    Log.e("removePost", "" + jsonObject);
                                    PostHelper postHelper = new PostHelper(mContext);
                                    return postHelper.Post(URLUtils.addWishlist, jsonObject.toString());
                                } catch (JSONException | IOException e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }

                            @Override
                            protected void onPostExecute(JSONObject jsonObject) {
                                super.onPostExecute(jsonObject);
                                Log.e("add_wishList", "" + jsonObject);
                                progress.setVisibility(View.GONE);
                                if (jsonObject != null) {
                                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {

                                    } else {
                                        UIUtils.showToastMsg(mContext, jsonObject.optString("error"));
                                        if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                                            Intent intent = new Intent(mContext, Login.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            mContext.startActivity(intent);
                                        }
                                    }
                                } else {

                                    UIUtils.showToast(mContext, R.string.con_timeout);
                                }

                            }
                        }.execute();
                        return false;
                    }
                });
                popupMenu.show();
                return false;
            }
        });*/

        /*holder.wishOptional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected ImageView wishImage, wishOptional;
        protected TextView wishName, wishTime, wishCate;


        public SingleItemRowHolder(View view) {
            super(view);

            this.wishImage = (ImageView) itemView.findViewById(R.id.wish_list_image);
            //this.wishOptional = (ImageView) itemView.findViewById(R.id.optional);
            this.wishName = (TextView) itemView.findViewById(R.id.wish_name);
            //this.wishTime = (TextView) itemView.findViewById(R.id.wish_time);
            //this.wishCate = (TextView) itemView.findViewById(R.id.wish_category);


        }

    }

}
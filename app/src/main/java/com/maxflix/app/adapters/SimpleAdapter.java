package com.maxflix.app.adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.maxflix.app.Activities.SingleVideoPage;
import com.maxflix.app.R;
import com.maxflix.app.models.SearchList;

import java.util.List;

public class SimpleAdapter extends
        RecyclerView.Adapter<SimpleAdapter.MyViewHolder> {

    private List<SearchList> list_item;
    public Context mcontext;

    public SimpleAdapter(List<SearchList> list, Context context) {

        list_item = list;
        mcontext = context;
    }

    // Called when RecyclerView needs a new RecyclerView.ViewHolder of the given type to represent an item.
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(mcontext).inflate(R.layout.movie_list_item, parent, false);
        return new MyViewHolder(view);
    }

    // Called by RecyclerView to display the data at the specified position.
    @Override
    public void onBindViewHolder(final MyViewHolder viewHolder, @SuppressLint("RecyclerView") final int position) {

        viewHolder.movie_name.setText(list_item.get(position).getMovie_name());
        viewHolder.movie_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String video_ID = list_item.get(position).getId();
                Intent intent = new Intent(mcontext, SingleVideoPage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("videoID", video_ID);
                mcontext.startActivity(intent);
            }
        });

    }

    // initializes some private fields to be used by RecyclerView.
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView movie_name;

        public MyViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            movie_name = (TextView) itemLayoutView.findViewById(R.id.movie_name);
        }
    }

    //Returns the total number of items in the data set hold by the adapter.
    @Override
    public int getItemCount() {
        return list_item.size();
    }

}
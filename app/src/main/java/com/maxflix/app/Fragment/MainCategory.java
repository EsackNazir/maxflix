package com.maxflix.app.Fragment;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.maxflix.app.Activities.Login;
import com.maxflix.app.R;
import com.maxflix.app.Utils.ConnectionHelper;
import com.maxflix.app.Utils.PostHelper;
import com.maxflix.app.Utils.SharedPref;
import com.maxflix.app.Utils.UIUtils;
import com.maxflix.app.Utils.URLUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class MainCategory extends Fragment {

    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private MainCateAdapter mainCateAdapter;


    public MainCategory() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_category, container, false);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
        recyclerView = (RecyclerView) view.findViewById(R.id.main_category);

        getMainCategory();

        return view;
    }

    private void getMainCategory() {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {
            JSONArray category_array;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(getActivity(), "ID"));
                    jsonObject.put("token", SharedPref.getKey(getActivity(), "TOKEN"));
                    PostHelper postHelper = new PostHelper(getActivity());
                    return postHelper.Post(URLUtils.categories, jsonObject.toString());
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                progressBar.setVisibility(View.GONE);
                Log.e("mainCategory", "" + jsonObject);
                if (jsonObject != null) {
                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {
                        category_array = jsonObject.optJSONArray("categories");
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setHasFixedSize(true);
                        mainCateAdapter = new MainCateAdapter(category_array);
                        recyclerView.setAdapter(mainCateAdapter);
                    } else {
                        UIUtils.showToastMsg(getActivity(), jsonObject.optString("error"));
                        if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                            Intent intent = new Intent(getActivity(), Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }

                } else {
                    UIUtils.showToast(getActivity(), R.string.con_timeout);
                }
            }
        }.execute();
    }

    private class MainCateAdapter extends RecyclerView.Adapter<MainCateAdapter.MyViewHolder> {
        JSONArray dataSet;

        public MainCateAdapter(JSONArray category_array) {
            this.dataSet = category_array;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.main_category_list_item, parent, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            holder.m_cate_name.setText(dataSet.optJSONObject(position).optString("name"));
            Glide.with(getActivity()).load(dataSet.optJSONObject(position).optString("picture")).centerCrop().error(R.drawable.ic_profile).crossFade(50).into(holder.m_cate_image);

        }

        @Override
        public int getItemCount() {
            return dataSet.length();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView m_cate_name;
            ImageView m_cate_image;

            public MyViewHolder(View itemView) {
                super(itemView);

                m_cate_name = (TextView) itemView.findViewById(R.id.main_cate_name);
                m_cate_image = (ImageView) itemView.findViewById(R.id.main_cate_image);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String id = dataSet.optJSONObject(getAdapterPosition()).optString("id");

                        Fragment fragment = new CommonCateFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("id", id);
                        bundle.putString("name", dataSet.optJSONObject(getAdapterPosition()).optString("name"));
                        fragment.setArguments(bundle);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, fragment);
                        transaction.commit();


                    }
                });
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ConnectionHelper.isConnectingToInternet(getActivity());
    }
}

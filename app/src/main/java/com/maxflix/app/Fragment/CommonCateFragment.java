package com.maxflix.app.Fragment;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.maxflix.app.Activities.Login;
import com.maxflix.app.R;
import com.maxflix.app.Utils.ConnectionHelper;
import com.maxflix.app.Utils.PostHelper;
import com.maxflix.app.Utils.SharedPref;
import com.maxflix.app.Utils.UIUtils;
import com.maxflix.app.Utils.URLUtils;
import com.maxflix.app.adapters.RecyclerViewDataAdapter;
import com.maxflix.app.models.SectionDataModel;
import com.maxflix.app.models.SingleItemModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class CommonCateFragment extends Fragment {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private RelativeLayout relativeLayout;
    private ArrayList<SectionDataModel> allSampleData;
    private String ID, sub_cate="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.comman_cate_fragment, container, false);

        Bundle bundle = getArguments();
        if (bundle != null) {
            ID = bundle.getString("id");
            String name = bundle.getString("name");
            sub_cate = bundle.getString("sub_cate");
            Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            toolbar.setTitle(name);
        }

        allSampleData = new ArrayList<SectionDataModel>();
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.error_layout);
        relativeLayout.setVisibility(View.GONE);
        recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);

        new AsyncTask<JSONObject, JSONObject, JSONObject>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(getActivity(), "ID"));
                    jsonObject.put("token", SharedPref.getKey(getActivity(), "TOKEN"));
                    jsonObject.put("category_id", ID);
                    Log.e("CatePost", "" + jsonObject);

                    PostHelper postHelper = new PostHelper(getActivity());
                    return postHelper.Post(URLUtils.categoryVideos, jsonObject.toString());
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                progressBar.setVisibility(View.GONE);
                Log.e("myList", "" + jsonObject);
                if (jsonObject != null) {
                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {
                        //ArrayList<HashMap<String, String>> getWishList = new ArrayList<>();
                        JSONArray mainArray = jsonObject.optJSONArray("data");
                        if (mainArray.length() == 0) {
                            relativeLayout.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        } else {

                            for (int i = 0; i < mainArray.length(); i++) {
                                JSONObject wishObj = mainArray.optJSONObject(i);
                                SectionDataModel dm = new SectionDataModel();
                                dm.setHeaderTitle(wishObj.optString("sub_category_name"));
                                dm.setKey(wishObj.optString("key"));
                                JSONArray innerArray = wishObj.optJSONArray("videos");
                                ArrayList<SingleItemModel> singleItem = new ArrayList<SingleItemModel>();
                                for (int j = 0; j < innerArray.length(); j++) {
                                    JSONObject object = innerArray.optJSONObject(j);
                                    String admin_video_id = object.optString("admin_video_id");
                                    String publish_time = object.optString("publish_time");
                                    String watch_count = object.optString("watch_count");
                                    String title = object.optString("title");
                                    String description = object.optString("description");
                                    String default_image = object.optString("default_image");
                                    String category_name = object.optString("category_name");

                                    singleItem.add(new SingleItemModel(admin_video_id, publish_time, watch_count,
                                            title, description, default_image, category_name, ""));
                                }
                                dm.setAllItemsInSection(singleItem);
                                allSampleData.add(dm);
                            }
                           /* if (getWishList.isEmpty()) {
                                relativeLayout.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                            } else {*/
                            recyclerView.setHasFixedSize(true);
                            RecyclerViewDataAdapter adapter = new RecyclerViewDataAdapter(getActivity(), allSampleData,sub_cate);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                            recyclerView.setItemAnimator(new DefaultItemAnimator());
                            recyclerView.setAdapter(adapter);
                            //}
                        }

                    } else {
                        UIUtils.showToastMsg(getActivity(), jsonObject.optString("error"));
                        if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                            Intent intent = new Intent(getActivity(), Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }
                } else {
                    UIUtils.showToast(getActivity(), R.string.con_timeout);
                }
            }
        }.execute();


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ConnectionHelper.isConnectingToInternet(getActivity());
    }
}

package com.maxflix.app.Fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.maxflix.app.Activities.Login;
import com.maxflix.app.Activities.PrivacyActivity;
import com.maxflix.app.R;
import com.maxflix.app.Utils.CustomDialog;
import com.maxflix.app.Utils.PostHelper;
import com.maxflix.app.Utils.SharedPref;
import com.maxflix.app.Utils.UIUtils;
import com.maxflix.app.Utils.URLUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


public class SettingsFragment extends Fragment implements View.OnClickListener {

    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);

        String push_status = SharedPref.getKey(getActivity(), "push_status");
        SwitchCompat notification_switch = (SwitchCompat) view.findViewById(R.id.notification_toggle);
        if (push_status.equalsIgnoreCase("1")) {
            notification_switch.setChecked(true);
        } else if (push_status.equalsIgnoreCase("0")) {
            notification_switch.setChecked(false);
        }
        notification_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton.isChecked()) {
                    notificationMethod(progressBar, "1");
                } else {
                    notificationMethod(progressBar, "0");
                }
            }
        });

        TextView privacy, terms, delete;
        privacy = (TextView) view.findViewById(R.id.privacy);
        privacy.setOnClickListener(this);
        terms = (TextView) view.findViewById(R.id.terms);
        terms.setOnClickListener(this);
        delete = (TextView) view.findViewById(R.id.delete_account);
        delete.setOnClickListener(this);


        return view;
    }

    private void notificationMethod(final ProgressBar progressBar, final String status) {

        new AsyncTask<JSONObject, JSONObject, JSONObject>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(getActivity(), "ID"));
                    jsonObject.put("token", SharedPref.getKey(getActivity(), "TOKEN"));
                    jsonObject.put("status", status);
                    Log.e("notification_post", "" + jsonObject);
                    PostHelper postHelper = new PostHelper(getActivity());
                    return postHelper.Post(URLUtils.settings, jsonObject.toString());
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                progressBar.setVisibility(View.GONE);
                if (jsonObject != null) {
                    Log.e("status", "" + jsonObject);
                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {
                        UIUtils.showToastMsg(getActivity(), jsonObject.optString("message"));
                        SharedPref.putKey(getActivity(), "push_status", jsonObject.optString("push_status"));
                    } else {
                        UIUtils.showToastMsg(getActivity(), jsonObject.optString("error"));
                        if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                            Intent intent = new Intent(getActivity(), Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }
                } else {
                    UIUtils.showToast(getActivity(), R.string.con_timeout);
                }

            }
        }.execute();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.privacy:
                Intent intent = new Intent(getActivity(), PrivacyActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("title", "Privacy Policy");
                intent.putExtra("type", "1");
                startActivity(intent);
                break;
            case R.id.terms:
                Intent terms = new Intent(getActivity(), PrivacyActivity.class);
                terms.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                terms.putExtra("title", "Terms and Conditions");
                terms.putExtra("type", "2");
                startActivity(terms);
                break;
            case R.id.delete_account:

                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Are you sure?. You want to delete your account.");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialogInterface, int i) {
                        new AsyncTask<JSONObject, JSONObject, JSONObject>() {

                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                progressBar.setVisibility(View.VISIBLE);
                            }

                            @Override
                            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                                JSONObject jsonObject = new JSONObject();
                                try {
                                    jsonObject.put("id", SharedPref.getKey(getActivity(), "ID"));
                                    jsonObject.put("token", SharedPref.getKey(getActivity(), "TOKEN"));
                                    PostHelper postHelper = new PostHelper(getActivity());
                                    return postHelper.Post(URLUtils.deleteAccount, jsonObject.toString());
                                } catch (JSONException | IOException e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }

                            @Override
                            protected void onPostExecute(JSONObject jsonObject) {
                                super.onPostExecute(jsonObject);
                                progressBar.setVisibility(View.GONE);
                                if (jsonObject != null) {
                                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {
                                        dialogInterface.dismiss();
                                        UIUtils.showToastMsg(getActivity(), jsonObject.optString("message"));
                                        Intent intent = new Intent(getActivity(), Login.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                    } else {
                                        UIUtils.showToastMsg(getActivity(), jsonObject.optString("error"));
                                    }

                                } else {
                                    UIUtils.showToast(getActivity(), R.string.con_timeout);
                                }
                            }
                        }.execute();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                    }
                });
                builder.show();


                break;
        }
    }
}

package com.maxflix.app.Fragment;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.maxflix.app.Activities.ChangePassword;
import com.maxflix.app.Activities.Login;
import com.maxflix.app.R;
import com.maxflix.app.Utils.ConnectionHelper;
import com.maxflix.app.Utils.GetHelper;
import com.maxflix.app.Utils.SharedPref;
import com.maxflix.app.Utils.SingleTon;
import com.maxflix.app.Utils.UIUtils;
import com.maxflix.app.Utils.URLUtils;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Profile extends Fragment {

    private Boolean change_layout = true;
    private CircularImageView pro_image;
    private MultipartEntity entityBuilder;
    private LinearLayout edit_field_layout, show_field_layout;
    private EditText edt_f_name, edt_email, edt_phone;
    private ScrollView showLayout;
    private RelativeLayout error_layout;
    private TextView t_f_name, t_email, t_pass;
    private ProgressBar progressBar;
    private String path = "", user_id = "", user_token = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_profile, container, false);

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Profile");

        user_id = SharedPref.getKey(getActivity(), "ID");
        user_token = SharedPref.getKey(getActivity(), "TOKEN");

        /*----------show field-----------*/
        showLayout = (ScrollView) view.findViewById(R.id.full_layout);
        error_layout = (RelativeLayout) view.findViewById(R.id.error_layout);
        error_layout.setVisibility(View.GONE);

        edit_field_layout = (LinearLayout) view.findViewById(R.id.edit_filed);
        show_field_layout = (LinearLayout) view.findViewById(R.id.show_filed);
        edit_field_layout.setVisibility(View.GONE);

        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);

        /*----------show field-----------*/
        t_f_name = (TextView) view.findViewById(R.id.txt_f_name);
        t_email = (TextView) view.findViewById(R.id.txt_email);
        t_pass = (TextView) view.findViewById(R.id.txt_password);

        /*----------edit field-----------*/

        edt_f_name = (EditText) view.findViewById(R.id.f_ed_first_name);
        edt_email = (EditText) view.findViewById(R.id.f_ed_email);
        edt_phone = (EditText) view.findViewById(R.id.f_ed_phone);

        /*----------edit field layout-----------*/
        final TextInputLayout lay_f_name, lay_l_name, lay_email, lay_phone;
        lay_f_name = (TextInputLayout) view.findViewById(R.id.layout_f_f_name);
        lay_email = (TextInputLayout) view.findViewById(R.id.layout_f_email);
        lay_phone = (TextInputLayout) view.findViewById(R.id.layout_f_phone);


        Button button = (Button) view.findViewById(R.id.change_password);
        if (SharedPref.getKey(getActivity(), "CHANGE_PASS").equalsIgnoreCase("Hide")) {
            button.setVisibility(View.GONE);
        } else {
            button.setVisibility(View.VISIBLE);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), ChangePassword.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                }
            });
        }

        pro_image = (CircularImageView) view.findViewById(R.id.f_profile_image);
        if (ConnectionHelper.isConnectingToInternet(getActivity())) {
            new ProfileLoad().execute();
        } else {
            UIUtils.showNetworkAlert(getActivity(), R.string.check_network);
        }


        final Button pro_edit_btn = (Button) view.findViewById(R.id.profile_edit_btn);
        pro_edit_btn.setText(R.string.profile_edit);
        pro_edit_btn.setOnClickListener(new View.OnClickListener()

                                        {
                                            @Override
                                            public void onClick(View view) {
                                                if (change_layout) {
                                                    show_field_layout.setVisibility(View.GONE);
                                                    Animation animation = AnimationUtils.makeOutAnimation(getActivity(), true);
                                                    show_field_layout.setAnimation(animation);
                                                    edit_field_layout.setVisibility(View.VISIBLE);
                                                    Animation animation1 = AnimationUtils.makeInAnimation(getActivity(), true);
                                                    edit_field_layout.setAnimation(animation1);
                                                    pro_edit_btn.setText(R.string.profile_save);

                                                    pro_image.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            Intent intent = new Intent(Intent.ACTION_PICK);
                                                            intent.setType("image/*");
                                                            startActivityForResult(Intent.createChooser(intent, "Select Photo"), 1);
                                                        }
                                                    });
                                                    edt_email.setText(t_email.getText().toString());
                                                    edt_f_name.setText(t_f_name.getText().toString());
                                                    edt_phone.setText(t_pass.getText().toString());
                                                    change_layout = false;
                                                    pro_image.setEnabled(true);
                                                } else {

                                                    Boolean checkField = false;

                                                    final String str_get_email = edt_email.getText().toString();
                                                    final String str_get_phone = edt_phone.getText().toString();
                                                    final String str_get_fName = edt_f_name.getText().toString();

                                                    if ((!isValidEmail(str_get_email))) {
                                                        lay_email.setError("Required Field");
                                                        checkField = true;
                                                    }
                                                    if (edt_phone.getText().toString().matches("")) {
                                                        lay_phone.setError("Required Field");
                                                        checkField = true;
                                                    }
                                                    if (edt_f_name.getText().toString().matches("")) {
                                                        lay_f_name.setError("Required Field");
                                                        checkField = true;
                                                    }

                                                    if (!checkField) {
                                                        new AsyncTask<String, Void, String>() {

                                                            @Override
                                                            protected void onPreExecute() {
                                                                super.onPreExecute();
                                                                progressBar.setVisibility(View.VISIBLE);
                                                            }

                                                            @SuppressWarnings("deprecation")
                                                            @Override
                                                            protected String doInBackground(String... params) {
                                                                try {
                                                                    HttpClient client = new DefaultHttpClient();
                                                                    HttpPost httpPost = new HttpPost(URLUtils.profile_update);
                                                                    entityBuilder = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

                                                                    if (path.isEmpty()) {
                                                                        Log.e("file_path", "is Empty");
                                                                    } else {
                                                                        File file = new File(path);
                                                                        Log.e("doInBack_file", "" + file);
                                                                        entityBuilder.addPart("picture", new FileBody(file, "application/octet"));
                                                                    }

                                                                    entityBuilder.addPart("id", new StringBody(user_id));
                                                                    entityBuilder.addPart("token", new StringBody(user_token));
                                                                    entityBuilder.addPart("email", new StringBody(str_get_email));
                                                                    entityBuilder.addPart("mobile", new StringBody(str_get_phone));
                                                                    entityBuilder.addPart("device_token", new StringBody(SingleTon.getInstance().GCMKey));
                                                                    entityBuilder.addPart("name", new StringBody(str_get_fName));

                                                                    httpPost.setEntity(entityBuilder);
                                                                    HttpResponse response = client.execute(httpPost);
                                                                    HttpEntity httpEntity = response.getEntity();

                                                                    if (response.getStatusLine().getStatusCode() == 200) {
                                                                        String res = EntityUtils.toString(httpEntity);
                                                                        Log.e("res", "" + res);
                                                                        return res;
                                                                    } else {
                                                                        return "Error";
                                                                    }

                                                                } catch (IOException e) {
                                                                    e.printStackTrace();
                                                                }

                                                                return null;
                                                            }

                                                            @Override
                                                            protected void onPostExecute(String s) {
                                                                super.onPostExecute(s);
                                                                progressBar.setVisibility(View.GONE);

                                                                if (s.equalsIgnoreCase("Error")) {
                                                                    UIUtils.showNetworkAlert(getActivity(), R.string.con_timeout);
                                                                } else {
                                                                    try {
                                                                        JSONObject object = new JSONObject(s);
                                                                        Log.e("object", "" + object);
                                                                        String result = object.optString("success");
                                                                        if (result.equalsIgnoreCase("true")) {
                                                                            new ProfileLoad().execute();
                                                                            show_field_layout.setVisibility(View.VISIBLE);
                                                                            Animation animation1 = AnimationUtils.makeInAnimation(getActivity(), true);
                                                                            show_field_layout.setAnimation(animation1);
                                                                            edit_field_layout.setVisibility(View.GONE);
                                                                            Animation animation = AnimationUtils.makeOutAnimation(getActivity(), true);
                                                                            edit_field_layout.setAnimation(animation);
                                                                            pro_edit_btn.setText(R.string.profile_edit);
                                                                            change_layout = true;
                                                                            pro_image.setEnabled(false);
                                                                        } else {
                                                                            UIUtils.showToastMsg(getActivity(), object.optString("error_messages"));
                                                                        }
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }


                                                            }
                                                        }.execute();

                                                    }


                                                }

                                            }
                                        }

        );


        return view;
    }


    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onResume() {
        super.onResume();
        ConnectionHelper.isConnectingToInternet(getActivity());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null && requestCode == 1) {
            Uri filePath = data.getData();
            Cursor cursor = null;
            try {
                String[] proj = {MediaStore.Images.Media.DATA};
                cursor = getActivity().getContentResolver().query(filePath, proj, null, null, null);
                assert cursor != null;
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                path = cursor.getString(column_index);
                Log.e("path", "" + path);
                //Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), filePath);
                Glide.with(getActivity()).load(path).error(R.drawable.ic_profile).centerCrop().into(pro_image);
                //pro_image.setImageBitmap(bitmap);
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
    }

    private class ProfileLoad extends AsyncTask<JSONObject, JSONObject, JSONObject> {
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected JSONObject doInBackground(JSONObject... jsonObjects) {
            GetHelper getHelper = new GetHelper(getActivity());
            try {
                return getHelper.run(URLUtils.base + "userApi/userDetails?id=" + user_id + "&token=" + user_token);
            } catch (IOException | JSONException e) {
                e.printStackTrace();

            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject s) {
            super.onPostExecute(s);
            progressBar.setVisibility(View.GONE);
            Log.e("Response", "" + s);
            if (s != null) {

                String result = s.optString("success");
                if (result.equalsIgnoreCase("true")) {
                    t_f_name.setText(s.optString("name"));
                    t_email.setText(s.optString("email"));
                    t_pass.setText(s.optString("mobile"));
                    TextView headerName = (TextView) getActivity().findViewById(R.id.profile_name);
                    headerName.setText(s.optString("name"));
                    CircularImageView imageView = (CircularImageView) getActivity().findViewById(R.id.profile_image);
                    Glide.with(getActivity()).load(s.optString("picture")).error(R.drawable.ic_profile).centerCrop().into(pro_image);
                    Glide.with(getActivity()).load(s.optString("picture")).error(R.drawable.ic_profile).centerCrop().into(imageView);

                    SharedPref.putKey(getActivity(), "NAME", s.optString("name"));
                    SharedPref.putKey(getActivity(), "PROFILE_IMAGE", s.optString("picture"));


                } else {
                    UIUtils.showToastMsg(getActivity(), s.optString("error"));
                    if (s.optString("error_code").equalsIgnoreCase("104")) {
                        Intent intent = new Intent(getActivity(), Login.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                }
            } else {
                showLayout.setVisibility(View.GONE);
                error_layout.setVisibility(View.VISIBLE);
            }

        }
    }
}

package com.maxflix.app.Fragment;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.maxflix.app.Activities.Login;
import com.maxflix.app.Activities.SearchActivity;
import com.maxflix.app.Activities.SingleVideoPage;
import com.maxflix.app.R;
import com.maxflix.app.Utils.ConnectionHelper;
import com.maxflix.app.Utils.PostHelper;
import com.maxflix.app.Utils.SharedPref;
import com.maxflix.app.Utils.UIUtils;
import com.maxflix.app.Utils.URLUtils;
import com.maxflix.app.adapters.RecyclerViewDataAdapter;
import com.maxflix.app.models.SectionDataModel;
import com.maxflix.app.models.SingleItemModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import me.relex.circleindicator.CircleIndicator;

public class CommonFragment extends Fragment {

    private RecyclerView recyclerView, searchList;
    private ProgressBar progressBar;
    private RelativeLayout relativeLayout;
    private ArrayList<SectionDataModel> allSampleData;
    private String sub_cate = "";

    private CircleIndicator circleIndicator;
    private ViewPager viewPager;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_common, container, false);
        allSampleData = new ArrayList<SectionDataModel>();
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.error_layout);
        relativeLayout.setVisibility(View.GONE);
        recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);

        circleIndicator = (CircleIndicator) view.findViewById(R.id.view_pager_indicator);
        viewPager = (ViewPager) view.findViewById(R.id.banner);

        new AsyncTask<JSONObject, JSONObject, JSONObject>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(getActivity(), "ID"));
                    jsonObject.put("token", SharedPref.getKey(getActivity(), "TOKEN"));

                    PostHelper postHelper = new PostHelper(getActivity());
                    return postHelper.Post(URLUtils.home, jsonObject.toString());
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                progressBar.setVisibility(View.GONE);
                Log.e("myList", "" + jsonObject);
                if (jsonObject != null) {
                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {
                        ArrayList<HashMap<String, String>> getWishList = new ArrayList<>();
                        JSONArray mainArray = jsonObject.optJSONArray("data");
                        JSONObject banner = jsonObject.optJSONObject("banner");
                        JSONArray banner_image_array = banner.optJSONArray("list");
                        if (banner_image_array != null && banner_image_array.length() > 0) {
                            ImagePagerAdapter adapter = new ImagePagerAdapter(banner_image_array);
                            viewPager.setAdapter(adapter);
                            circleIndicator.setViewPager(viewPager);
                        }
                        if (mainArray.length() == 0) {
                            relativeLayout.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        } else {

                            for (int i = 0; i < mainArray.length(); i++) {
                                JSONObject wishObj = mainArray.optJSONObject(i);
                                SectionDataModel dm = new SectionDataModel();
                                dm.setHeaderTitle(wishObj.optString("name"));
                                dm.setKey(wishObj.optString("key"));
                                JSONArray innerArray = wishObj.optJSONArray("list");
                                ArrayList<SingleItemModel> singleItem = new ArrayList<SingleItemModel>();
                                if (innerArray != null) {
                                    for (int j = 0; j < innerArray.length(); j++) {
                                        JSONObject object = innerArray.optJSONObject(j);
                                        String admin_video_id = object.optString("admin_video_id");
                                        String publish_time = object.optString("publish_time");
                                        String watch_count = object.optString("watch_count");
                                        String title = object.optString("title");
                                        String description = object.optString("description");
                                        String default_image = object.optString("default_image");
                                        String category_name = object.optString("category_name");
                                        String wishlist_id = object.optString("wishlist_id");

                                        singleItem.add(new SingleItemModel(admin_video_id, publish_time, watch_count,
                                                title, description, default_image, category_name, wishlist_id));
                                    }
                                }
                                dm.setAllItemsInSection(singleItem);
                                allSampleData.add(dm);
                            }
                            /*if (getWishList.isEmpty()) {
                                relativeLayout.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                            } else {*/

                            recyclerView.setHasFixedSize(true);
                            RecyclerViewDataAdapter adapter = new RecyclerViewDataAdapter(getActivity(), allSampleData, sub_cate);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                            recyclerView.setAdapter(adapter);

                            // }
                        }

                    } else {
                        UIUtils.showToastMsg(getActivity(), jsonObject.optString("error"));
                        if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                            Intent intent = new Intent(getActivity(), Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }
                } else {
                    UIUtils.showToast(getActivity(), R.string.con_timeout);
                }
            }
        }.execute();


        return view;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_menu, menu);

        MenuItem menuItem = menu.findItem(R.id.search_bar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getActivity(), SearchActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onResume() {
        super.onResume();
        ConnectionHelper.isConnectingToInternet(getActivity());
    }

    private class ImagePagerAdapter extends PagerAdapter {
        private JSONArray imageArray;

        public ImagePagerAdapter(JSONArray banner_image_array) {
            this.imageArray = banner_image_array;
        }

        @Override
        public int getCount() {
            return imageArray.length();
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            final ImageView imageView = new ImageView(getActivity());
            imageView.setImageResource(imageArray.length());
            Glide.with(getActivity()).load(imageArray.optJSONObject(position).optString("default_image")).centerCrop().crossFade(90).error(R.drawable.ic_profile).into(imageView);
            container.addView(imageView, 0);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String videoID = imageArray.optJSONObject(position).optString("admin_video_id");
                    Intent intent = new Intent(getActivity(), SingleVideoPage.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("videoID", videoID);
                    startActivity(intent);
                }
            });

            return imageView;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((ImageView) object);
        }

    }
}
